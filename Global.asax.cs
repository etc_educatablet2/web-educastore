﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace WebEducaStore
{
    // Nota: para obtener instrucciones sobre cómo habilitar el modo clásico de IIS6 o IIS7, 
    // visite http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        LogEntry logEntry = ETC.BL.Utilities.Utils.BuildLoggerObject(typeof(MvcApplication).Name, ETC.BL.Common.PROJECT_ENUM.ETC_BL);

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            Utils.Utility.InitializeLogger();
        }

        /// <summary>
        /// Dont allow back button when logout
        /// Every project called from this project have to implement this method
        /// </summary>
        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();

            //culture information for payments
            //System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            CultureInfo ci = new CultureInfo(System.Threading.Thread.CurrentThread.CurrentCulture.ToString());
            ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            Logger.Write(logEntry.Message = e.ToString());

            ETC.BL.Exceptions.ETCException etcException = ETC.BL.Utilities.Utils.BuildETCException(this.GetType().Name,
                                                  string.Empty,
                                                  MethodBase.GetCurrentMethod().Name,
                                                  "An unhandled exception has ocurred",
                                                  ETC.BL.Common.PROJECT_ENUM.ETC_BL,
                                                  2,
                                                  ex);

            //ETC.BL.Utilities.Utils.BuildLoggerObject(etcException, System.Diagnostics.TraceEventType.Error);
        }
    }
}