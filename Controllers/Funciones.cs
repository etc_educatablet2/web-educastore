﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace WebEducaStore.Controllers
{
  /// <summary>
  /// Summary description for Funciones
  /// </summary>
  public class Funciones
  {
    #region Definición de Variables de la clase
    public int intCodigoError;
    public String strDescripcionError;
    #endregion

    public Funciones()
    {
    }

    //FUNCIONES PARA CONEXION CON BASE DE DATOS
    #region "FUNCIONES DE BASE DE DATOS"
    public SqlConnection CrearConexionBase()
    {
      string strConn = "";
      SqlConnection Conn;

      try
      {
        strConn = ConfigurationManager.ConnectionStrings["strConnDBBase"].ToString();
        Conn = new SqlConnection(strConn);
        Conn.Open();
        return Conn;
      }
      catch(Exception ex)
      {
        intCodigoError = 1;
        strDescripcionError = ex.Message;
        //SE INSERTA EN EL LOG DEL SERVICIO
        return null;
      }
    }

    public SqlConnection CrearConexionPersonas()
    {
      string strConn = "";
      SqlConnection Conn;

      try
      {
        strConn = ConfigurationManager.ConnectionStrings["strConnDBPersonas"].ToString();
        Conn = new SqlConnection(strConn);
        Conn.Open();
        return Conn;
      }
      catch(Exception ex)
      {
        intCodigoError = 1;
        strDescripcionError = ex.Message;
        //SE INSERTA EN EL LOG DEL SERVICIO
        return null;
      }
    }

    public SqlConnection CrearConexionEmpresas()
    {
      string strConn = "";
      SqlConnection Conn;

      try
      {
        strConn = ConfigurationManager.ConnectionStrings["strConnDBEmpresas"].ToString();
        Conn = new SqlConnection(strConn);
        Conn.Open();
        return Conn;
      }
      catch(Exception ex)
      {
        intCodigoError = 1;
        strDescripcionError = ex.Message;
        //SE INSERTA EN EL LOG DEL SERVICIO
        return null;
      }
    }

    public OleDbConnection CrearConexionOracle()
    {
      string strConn = "";
      OleDbConnection Conn;

      try
      {
        strConn = ConfigurationManager.ConnectionStrings["strConnDBLegacyOracle"].ToString();
        Conn = new OleDbConnection(strConn);
        Conn.Open();
        return Conn;
      }
      catch(Exception ex)
      {
        intCodigoError = 1;
        strDescripcionError = ex.Message;
        //SE INSERTA EN EL LOG DEL SERVICIO
        return null;
      }
    }

    public OleDbConnection CrearConexionLegacy()
    {
      string strConn = "";
      OleDbConnection Conn;

      try
      {
        strConn = ConfigurationManager.ConnectionStrings["strConnDBLegacy"].ToString();
        Conn = new OleDbConnection(strConn);
        Conn.Open();
        return Conn;
      }
      catch(Exception ex)
      {
        intCodigoError = 1;
        strDescripcionError = ex.Message;
        //SE INSERTA EN EL LOG DEL SERVICIO
        return null;
      }
    }

    public void cerrarConexion(SqlConnection Conn)
    {
      if(Conn != null)
      {
        if(Conn.State == ConnectionState.Open)
          Conn.Close();
      }
    }

    public void cerrarConexionLegacy(OleDbConnection Conn)
    {
      if(Conn != null)
      {
        if(Conn.State == ConnectionState.Open)
          Conn.Close();
      }
    }

    #endregion


    # region "FUNCIONES GENERICAS"

    public string GeneraPwdUsuarioNuevo(string strCI, string strNombre, string strApellido)
    {
      string strNuevoPwd = "";
      //strNuevoPwd = Convert.ToChar(Convert.ToChar(strNombre.Substring(0,1)) + 1).ToString();
      strNuevoPwd = Convert.ToString(Convert.ToChar(strNombre.Substring(0, 1)) + 1);
      strNuevoPwd += Convert.ToString(Convert.ToChar(strCI.Substring(0, 1)) + 2);
      for(int i = strCI.Length - 1; i > 7; i--)
      {
        strNuevoPwd += Convert.ToInt16(strCI.Substring(i, 1)) + 1;
      }
      strNuevoPwd += Convert.ToString(Convert.ToChar(strApellido.Substring(0, 1)) - 1);
      return strNuevoPwd;

    }



    //Función utilizada para obtener de la tabla CP_SECUENCIAL a partir
    //de los parametros de entrada,el secuencial que se insertara en las
    //tablas que lo necesiten

    public int Buscar_Secuencial(string Tabla, string Campo, string Sistema)
    {
      string strSql;
      int intSecuencial;
      string SqlUpdate;
      SqlConnection ConnSec = new SqlConnection();
      SqlCommand cmdStrSql = new SqlCommand();
      SqlTransaction myTransSec;
      SqlDataReader dr;

      // ABRIENDO CONEXION CON LA BASE DE DATOS
      ConnSec = CrearConexionBase();
      myTransSec = ConnSec.BeginTransaction();
      try
      {
        strSql = " Select CS_secuencial FROM C_SECUENCIAL ";
        strSql = strSql + " WHERE ltrim(rtrim(CS_nombre_tabla))= '" + Tabla + "' ";
        strSql = strSql + " and ltrim(rtrim(CS_nombre_campo))= '" + Campo + "' ";
        strSql = strSql + " and ltrim(rtrim(CS_SISTEMA)) = '" + Sistema + "' ";


        cmdStrSql = new SqlCommand(strSql, ConnSec);

        cmdStrSql.Transaction = myTransSec;

        intSecuencial = Convert.ToInt32(cmdStrSql.ExecuteScalar().ToString()) + 1;

        SqlUpdate = "Update C_SECUENCIAL ";
        SqlUpdate = SqlUpdate + " SET CS_SECUENCIAL= '" + intSecuencial + "' ";
        SqlUpdate = SqlUpdate + " WHERE ltrim(rtrim(CS_NOMBRE_TABLA))= '" + Tabla + "' ";
        SqlUpdate = SqlUpdate + " and ltrim(rtrim(CS_nombre_campo))= '" + Campo + "' ";
        SqlUpdate = SqlUpdate + " and ltrim(rtrim(CS_SISTEMA)) = '" + Sistema + "' ";

        cmdStrSql.CommandText = SqlUpdate;
        cmdStrSql.ExecuteNonQuery();
        myTransSec.Commit();
        ConnSec.Close();
        return intSecuencial - 1;
      }
      catch
      {
        myTransSec.Rollback();
        ConnSec.Close();
        return 0;
      }
    }

    public void ReDim(ref string[,] arr, int row, int col)
    {
      string[,] arrTemp = new string[row, col];
      Array.Copy(arr, 0, arrTemp, 0, arr.Length);
      arr = arrTemp;
    }

    public bool insertarLogTransacciones(dataLogTransacciones dataTransacciones)
    {
      int intIdRegistro;
      string strSQL;


      try
      {


        // BUSCA EL ID CLAVE QUE SE INSERTARA EN LA TABLA
        intIdRegistro = Buscar_Secuencial("CP_COLA", "CPC_ID", "CP");

        // INGRESA REGISTRO EN BASE DE DATOS
        // QUERY DE LA BASE DE DATOS
        strSQL = "";
        strSQL = "INSERT INTO CP_COLA ( ";
        strSQL += "  CPC_ID, ";
        strSQL += "  CPC_OPER_DATE, ";
        strSQL += "  CPC_USERID, ";
        strSQL += "  CPC_OPER_CODE, ";
        strSQL += "  CPC_OBJECT_1, "; // strCuentaAcreditar
        strSQL += "  CPC_OBJECT_2, "; // strCuentaAcreditar
        strSQL += "  CPC_OBJECT_3, "; // strTipoCuentaDebito
        strSQL += "  CPC_OBJECT_4, "; // strTipoTarjeta
        strSQL += "  CPC_OBJECT_5, "; // strCedulaBeneficiario
        strSQL += "  CPC_OBJECT_6, "; // strNombreBeneficiario
        strSQL += "  CPC_OBJECT_7, "; // strEmailBeneficiario
        strSQL += "  CPC_TICKET_NUMBER, "; // strTicketNumber
        strSQL += "  CPC_OPER_STATUS, "; // strStatus
        strSQL += "  CPC_OPER_AMOUNT, ";
        strSQL += "  CPC_VALUE_DATE, ";
        strSQL += "  CPC_DESCRIPTION, ";
        strSQL += "  CPC_BANK_CODE, ";
        strSQL += "  CPC_DESCRIPTION_ERROR, ";
        strSQL += "  CPC_COMMISSION, ";
        strSQL += "  CPC_TAXES,";
        strSQL += "  ADDRESS_IP_CLIENT,";
        strSQL += "  TYPE_BROWSER_INFO)";
        strSQL += " VALUES(";
        strSQL += "'" + intIdRegistro.ToString() + "', ";
        strSQL += " GETDATE(),";
        strSQL += "'" + dataTransacciones.strUserCode + "', ";
        strSQL += "'" + dataTransacciones.strOperationCode + "', ";
        strSQL += "'" + dataTransacciones.strObject1 + "', ";
        strSQL += "'" + dataTransacciones.strObject2 + "', ";
        strSQL += "'" + dataTransacciones.strObject3 + "', ";
        strSQL += "'" + dataTransacciones.strObject4 + "', ";
        strSQL += "'" + dataTransacciones.strObject5 + "', ";
        strSQL += "'" + dataTransacciones.strObject6 + "', ";
        strSQL += "'" + dataTransacciones.strObject7 + "', ";
        strSQL += "'" + dataTransacciones.strTicketNumer + "', ";
        strSQL += "'" + dataTransacciones.strOperationStatus + "', ";
        strSQL += " " + dataTransacciones.dblOperatioAmount.ToString().Replace(',', '.') + ", ";
        strSQL += " CONVERT(datetime,'" + dataTransacciones.dateOperationValueDate.ToString() + "'), ";
        strSQL += "'" + dataTransacciones.strDescription + "', ";
        strSQL += "'" + dataTransacciones.strBankCode + "', ";
        strSQL += "'" + dataTransacciones.strDescriptionError + "', ";
        strSQL += " " + dataTransacciones.dblCommission.ToString().Replace(',', '.') + " , ";
        strSQL += " " + dataTransacciones.dblTaxes.ToString().Replace(',', '.') + " , ";
        strSQL += "'" + dataTransacciones.strUserIP + "', ";
        strSQL += "'" + dataTransacciones.strUserBrowserInfo + "')";

        // ABRIENDO CONEXION CON LA BASE DE DATOS
        SqlConnection Conn = new SqlConnection();
        Conn = CrearConexionPersonas();

        // EJECUTANDO EL QUERY EN LA BASE DE DATOS
        SqlCommand CMD = new SqlCommand(strSQL, Conn);

        // ASIGNANDO EL RESULTADO A UN DATA-READER
        if(CMD.ExecuteNonQuery() < 1)
        {
          Conn.Close();
          return false;
        }
        else
        {
          Conn.Close();
          return true;
        }
      }
      catch(Exception ex)
      {
        return false;
      }
    }

    /// <summary>
    /// Estructura para insertar en log de Transacciones CP_COLA
    /// </summary>
    public struct dataLogTransacciones
    {
      public string strUserCode;
      public string strOperationCode;
      public string strObject1;
      public string strObject2;
      public string strObject3;
      public string strObject4;
      public string strObject5;
      public string strObject6;
      public string strObject7;
      public string strTicketNumer;
      public string strOperationStatus;
      public double dblOperatioAmount;
      public string dateOperationValueDate;
      public string strDescription;
      public string strBankCode;
      public string strDescriptionError;
      public double dblCommission;
      public double dblTaxes;
      public string strUserIP;
      public string strUserBrowserInfo;

    };



    #endregion


    #region "FUNCIONES DE VALIDACION"

    /// <summary>
    /// VERIFICA QUE LA FECHA DADA NO ESTE EN LA TABLA DE DIAS FERIADOS
    /// </summary>
    /// <param name="dtFecha">FECHA A VERIFICAR</param>
    /// <returns>RETORNA false SI LA FECHA NO ESTA EN LA TABLA Y true SI ES DIA FERIADO</returns>



    /// <summary>
    /// CALCULA UNA FECHA NUEVA EN BASE A PARAMETRO ENVIADO
    /// </summary>
    /// <param name="dtFecha">FECHA A VERIFICAR</param>
    /// <param name="strParametro">PARAMETRO A CONSIDERAR PARA EL CALCULO DE LA NUEVA FECHA</param>
    /// <returns>FECHA SUGERIDA</returns>
    public DateTime CalcularProximaFechaValida(DateTime dtFecha, string strParametro)
    {
      DateTime stFechaPropuesta = dtFecha;
      int intDia = stFechaPropuesta.Day;
      int intMes = stFechaPropuesta.Month;
      int intAno = stFechaPropuesta.Year;

      try
      {
        if(strParametro == "FINDESEMANA")
        {
          if(stFechaPropuesta.DayOfWeek.ToString() == "Saturday" || stFechaPropuesta.DayOfWeek.ToString() == "Sabado")
            stFechaPropuesta = stFechaPropuesta.AddDays(2);
          else
            stFechaPropuesta = stFechaPropuesta.AddDays(1);
        }
        else if(strParametro == "DIAFERIADO")
        {
          stFechaPropuesta = stFechaPropuesta.AddDays(1);
        }
        intCodigoError = 0;
        strDescripcionError = "";
        return stFechaPropuesta;
      }
      catch(Exception exp)
      {
        intCodigoError = 1;
        strDescripcionError = exp.Message;
        return dtFecha;
      }
    }

    /// <summary>
    /// VERIFICA QUE LA FECHA DADA SEA UNA FECHA VALIDA PARA OPERACIONES A OTROS BANCOS
    /// </summary>
    /// <param name="dtFecha">FECHA A VERIFICAR</param>
    /// <returns>FECHA VALIDA</returns>



    /// <summary>
    /// VERIFICA SI LA HORA ESTA ENTRE EL RANGO OPERATIVO DEL BANCO
    /// </summary>
    /// <param name="IdModulo">ID DEL MODULO QUE SE ESTA EVALUANDO</param>
    /// <param name="strHoraActual">HORA ACTUAL EN QUE SE EJECUTA LA OPERACION</param>
    /// <returns>RETONA false SI LA HORA NO ESTA EN EL RANGO OPERATIVO DEL BANCO</returns>




    /// <summary>
    /// BUSCA TODOS LOS MONTOS DE RANGOS EN LA TABLA DE MONTOS DADO EL ID DE LA TRANSACCION
    /// </summary>
    /// <param name="IdModulo">ID DE LA TRANSACCION</param>
    /// <returns>STRING SEPARANDO CON COMAS TODOS LOS CAMPOS DE RANGOS DE MONTOS</returns>


    /// <summary>
    /// BUSCA EL MONTO ACUMULADO EN EL DIA POR TIPO DE TRANSACCION
    /// </summary>
    /// <param name="IdModulo">ID DEL MODULO DE LA TRANSACCION</param>
    /// <param name="IdUsuario">ID DEL USUARIO</param>
    /// <param name="dtFechaValor">FECHA VALOR EN LA QUE CORRERA LA TRANSCCION</param>
    /// <returns>RETORNA EL MONTO CON EL MONTO ACUMULADO</returns>
    public double BuscaMontoAcumuladoDia(string IdModulo, string IdUsuario, DateTime dtFechaValor)
    {

      Double dblMontoAcumulado = 0;
      //SqlConnection ConnP = new SqlConnection();

      try
      {
        // VARIABLES
        SqlDataReader drMontoAcumuladoDia;
        string strSQL;

        string FechaValorAno = dtFechaValor.Year.ToString();
        string FechaValorMes = dtFechaValor.Month.ToString();
        if(FechaValorMes.ToString().Length == 1)
          FechaValorMes = "0" + FechaValorMes;
        string FechaValorDia = dtFechaValor.Day.ToString();
        if(FechaValorDia.ToString().Length == 1)
          FechaValorDia = "0" + FechaValorDia;

        string FechaValorDef = FechaValorAno + FechaValorMes + FechaValorDia;

        switch(IdModulo)
        {
          case "TRNI":
            IdModulo = "TIL";
            break;
          case "TRNO":
            IdModulo = "TEL";
            break;

        }


        // QUERY DE LA BASE DE DATOS
        strSQL = "SELECT isnull(SUM(CPC_OPER_AMOUNT),0) AS ACUMULADO ";
        strSQL += " FROM CP_COLA ";
        strSQL += " WHERE CPC_USERID = '" + IdUsuario + "' ";
        strSQL += " AND CPC_OPER_CODE = '" + IdModulo + "' ";
        strSQL += " AND CPC_VALUE_DATE = convert(datetime,'" + FechaValorDef + "') ";
        strSQL += " AND CPC_OPER_STATUS not in ('002','005','018','019') ";

        // ABRIENDO CONEXION CON LA BASE DE DATOS
        SqlConnection Conn = new SqlConnection();
        Conn = CrearConexionPersonas();


        // EJECUTANDO EL QUERY EN LA BASE DE DATOS
        SqlCommand CMD = new SqlCommand(strSQL, Conn);

        // ASIGNANDO EL RESULTADO A UN DATA-READER
        drMontoAcumuladoDia = CMD.ExecuteReader();


        // DETERMINA SI TRAJO INFORMACION
        if(drMontoAcumuladoDia.Read())
        {
          dblMontoAcumulado = Convert.ToDouble(drMontoAcumuladoDia["ACUMULADO"]);
          // CERRAR EL DATA-READER
          drMontoAcumuladoDia.Close();
          // CERRAR CONEXION CON LA BASE DE DATOS
          cerrarConexion(Conn);
          return dblMontoAcumulado;

        }
        else
        {
          // CERRAR EL DATA-READER
          drMontoAcumuladoDia.Close();
          // CERRAR CONEXION CON LA BASE DE DATOS
          cerrarConexion(Conn);
          return dblMontoAcumulado = 0;
        }


      }
      catch(Exception ex)
      {
        // CERRAR CONEXION CON LA BASE DE DATOS
        //cerrarConexion(Conn);
        return dblMontoAcumulado = 0;
      }



    }





    /// <summary>
    /// BUSCA LA CANTIDAD DE TRANSACCIONES POR TIPO EN EL DIA
    /// </summary>
    /// <param name="IdModulo">ID DEL MODULO DE LA TRANSACCION</param>
    /// <param name="IdUsuario">ID DEL USUARIO</param>
    /// <param name="dtFechaValor">FECHA VALOR EN LA QUE CORRERA LA TRANSCCION</param>
    /// <returns>RETORNA LA CANTIDAD DE TRANSACCIOES ACUMULADAS</returns>
    public int BuscaCantidadTransaccionesDia(string IdModulo, string IdUsuario, DateTime dtFechaValor)
    {

      int intCantidadAcumulado = 0;
      //SqlConnection Conn = new SqlConnection();

      try
      {
        // VARIABLES
        SqlDataReader drCantidadAcumuladoDia;
        string strSQL;

        string FechaValorAno = dtFechaValor.Year.ToString();
        string FechaValorMes = dtFechaValor.Month.ToString();
        if(FechaValorMes.ToString().Length == 1)
          FechaValorMes = "0" + FechaValorMes;
        string FechaValorDia = dtFechaValor.Day.ToString();
        if(FechaValorDia.ToString().Length == 1)
          FechaValorDia = "0" + FechaValorDia;

        string FechaValorDef = FechaValorAno + FechaValorMes + FechaValorDia;

        switch(IdModulo)
        {
          case "TRNI":
            IdModulo = "TIL";
            break;
          case "TRNO":
            IdModulo = "TEL";
            break;

        }


        // QUERY DE LA BASE DE DATOS
        strSQL = "SELECT isnull(COUNT(CPC_OPER_AMOUNT),0) AS CANTIDAD ";
        strSQL += " FROM CP_COLA ";
        strSQL += " WHERE CPC_USERID = '" + IdUsuario + "' ";
        strSQL += " AND CPC_OPER_CODE = '" + IdModulo + "' ";
        strSQL += " AND CPC_VALUE_DATE = convert(datetime,'" + FechaValorDef + "') ";
        strSQL += " AND CPC_OPER_STATUS not in ('002','005','018','019') ";

        // ABRIENDO CONEXION CON LA BASE DE DATOS
        SqlConnection Conn = new SqlConnection();
        Conn = CrearConexionPersonas();

        // EJECUTANDO EL QUERY EN LA BASE DE DATOS
        SqlCommand CMD = new SqlCommand(strSQL, Conn);

        // ASIGNANDO EL RESULTADO A UN DATA-READER
        drCantidadAcumuladoDia = CMD.ExecuteReader();


        // DETERMINA SI TRAJO INFORMACION
        if(drCantidadAcumuladoDia.Read())
        {
          intCantidadAcumulado = Convert.ToInt32(drCantidadAcumuladoDia["CANTIDAD"]);
          // CERRAR EL DATA-READER
          drCantidadAcumuladoDia.Close();
          // CERRAR CONEXION CON LA BASE DE DATOS
          cerrarConexion(Conn);
          return intCantidadAcumulado;
        }
        else
        {
          // CERRAR EL DATA-READER
          drCantidadAcumuladoDia.Close();
          // CERRAR CONEXION CON LA BASE DE DATOS
          cerrarConexion(Conn);
          return intCantidadAcumulado = 0;
        }

      }
      catch(Exception ex)
      {

        // CERRAR CONEXION CON LA BASE DE DATOS
        //cerrarConexion(Conn);
        return intCantidadAcumulado = 0;
      }

    }


    #endregion

    #region "FUNCIONES DE BUSQUEDA DE INFORMACION"




    #endregion

    #region CREACION DE DATASET VACIOS
    public DataSet CrearDataSet(string strNombreTabla, string strCampos)
    {
      DataSet dtsNuevo = new DataSet();

      dtsNuevo.Tables.Add(strNombreTabla);
      string[] arrCampos = strCampos.Split(';');
      for(int i = 0; i < arrCampos.Length; i++)
      {
        dtsNuevo.Tables[strNombreTabla].Columns.Add(arrCampos[i]);
      }
      return dtsNuevo;

    }
    #endregion

    #region "Valida el monto de la transacción realizada, con el monto de aprobacion para la operación que se esta realizando"
    /// <summary>
    /// Valida el monto de la transacción realizada, con el monto de aprobacion para la operación que se esta realizando.
    /// </summary>
    /// <param name="dblMonto">Monto a validar</param>
    /// <param name="strSistema">Sistema</param>
    /// <param name="strOperacion">Operación</param>
    /// <returns>true es menor al monto de operación, false es mayor al monto de aprobación</returns>
    public bool ValidaMontosAprobacionBco(Double dblMonto, string strSistema, string strOperacion)
    {
      SqlConnection Conn = new SqlConnection();

      try
      {
        // VARIABLES
        Double dblMontoAprobadoBco;
        SqlDataReader drMontoAprobadoBco;
        string strSQL;

        // QUERY DE LA BASE DE DATOS
        strSQL = "SELECT CTR_BANK_APROVED_AMOUNT ";
        strSQL += " FROM C_TRANS_RULE ";
        strSQL += " WHERE CTR_SOURCE_SYSTEM = '" + strSistema + "' ";
        strSQL += " AND CTR_TRANS_CODE = '" + strOperacion + "' ";

        // ABRIENDO CONEXION CON LA BASE DE DATOS
        Conn = CrearConexionBase();

        // EJECUTANDO EL QUERY EN LA BASE DE DATOS
        SqlCommand CMD = new SqlCommand(strSQL, Conn);

        // ASIGNANDO EL RESULTADO A UN DATA-READER
        drMontoAprobadoBco = CMD.ExecuteReader();

        // DETERMINA SI TRAJO INFORMACION
        if(drMontoAprobadoBco.Read())
        {
          dblMontoAprobadoBco = Convert.ToDouble(drMontoAprobadoBco["CTR_BANK_APROVED_AMOUNT"]);
          // CERRAR EL DATA-READER
          drMontoAprobadoBco.Close();
          // CERRAR CONEXION CON LA BASE DE DATOS
          cerrarConexion(Conn);
          if(dblMonto > dblMontoAprobadoBco)
          {
            return false;
          }
          else
          {
            return true;
          }
        }
        else
        {
          // CERRAR EL DATA-READER
          drMontoAprobadoBco.Close();
          // CERRAR CONEXION CON LA BASE DE DATOS
          cerrarConexion(Conn);
          return true;
        }
      }
      catch(Exception ex)
      {
        // CERRAR CONEXION CON LA BASE DE DATOS
        cerrarConexion(Conn);
        return true;
      }


    }
    #endregion

    private string Key = "";
    private readonly byte[] IVector = new byte[8] { 27, 9, 45, 27, 0, 72, 171, 54 };


    public string Encrypt(string inputString)
    {

      byte[] buffer = Encoding.ASCII.GetBytes(inputString);
      TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider();
      MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
      tripleDes.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(Key));
      tripleDes.IV = IVector;
      ICryptoTransform ITransform = tripleDes.CreateEncryptor();
      return Convert.ToBase64String(ITransform.TransformFinalBlock(buffer, 0, buffer.Length));
    }

    public string Decrypt(string inputString)
    {
      byte[] buffer = Convert.FromBase64String(inputString);
      TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider();
      MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
      tripleDes.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(Key));
      tripleDes.IV = IVector;
      ICryptoTransform ITransform = tripleDes.CreateDecryptor();
      return Encoding.ASCII.GetString(ITransform.TransformFinalBlock(buffer, 0, buffer.Length));
    }

    public string ConvertDateTime(DateTime dtDate)
    {
      string strDateYear = dtDate.Year.ToString();
      string strDateMonth = dtDate.Month.ToString();
      if(strDateMonth.ToString().Length == 1)
        strDateMonth = "0" + strDateMonth;
      string strDateDay = dtDate.Day.ToString();
      if(strDateDay.ToString().Length == 1)
        strDateDay = "0" + strDateDay;

      string ReturnDate = strDateYear + "-" + strDateMonth + "-" + strDateDay + " " + dtDate.Hour + ":" + dtDate.Minute + ":" + dtDate.Second;

      return ReturnDate;


    }



    public string getLabel(string strLabel)
    {
      return ConfigurationManager.AppSettings[strLabel].ToString();
    }




    // Single-digit and small number names
    private string[] _smallNumbersSpa = new string[] { "Cero", "Uno", "Dos", "Tres", "Cuatro", "Cinco", "Seis", "Siete", "Ocho", "Nueve", "Diez", "Once", "Doce", "Trece", "Catorce", "Quince", "Dieciseis", "Diecisiete", "Dieciocho", "Diecinueve" };

    private string[] _smallNumbers = new string[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };

    // Tens number names from twenty upwards
    private string[] _tensSpa = new string[] { "", "", "Veinte", "Treinta", "Cuarenta", "Cincuenta", "Sesenta", "Setenta", "Ochenta", "Noventa" };

    private string[] _tens = new string[] { "", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

    // Scale number names for use during recombination
    private string[] _scaleNumbersSpa = new string[] { "", "Mil", "Millon", "Mil Millon" };

    private string[] _scaleNumbers = new string[] { "", "Thousand", "Million", "Billion" };


    public string ConvertNumberToWords(double numberDouble)
    {
      string frase = "";

      double temp1 = Math.Floor(numberDouble);

      double temp2 = numberDouble - temp1;

      double temp3 = Math.Round(temp2 * 100, 0);

      long fraction = (long) (temp3);

      long number = (long) numberDouble;

      frase = NumberToWords(number);

      string frasecentimos = "";

      if(fraction > 1)
        frasecentimos = "s";

      if(fraction > 0)
        frase += " and " + NumberToWords(fraction) + " Cent" + frasecentimos;

      return frase;

    }

    // Converts an integer value into English words
    public string NumberToWords(long number)
    {
      // Zero rule
      if(number == 0)
        return _smallNumbers[0];

      // Array to hold four three-digit groups
      long[] digitGroups = new long[4];

      // Ensure a positive number to extract from
      long positive = Math.Abs(number);

      // Extract the three-digit groups
      for(int i = 0; i < 4; i++)
      {
        digitGroups[i] = positive % 1000;
        positive /= 1000;
      }

      // Convert each three-digit group to words
      string[] groupText = new string[4];

      for(int i = 0; i < 4; i++)
        groupText[i] = ThreeDigitGroupToWords(digitGroups[i]);

      // Recombine the three-digit groups
      string combined = groupText[0];
      bool appendAnd;

      // Determine whether an 'and' is needed
      appendAnd = (digitGroups[0] > 0) && (digitGroups[0] < 100);

      // Process the remaining groups in turn, smallest to largest
      for(int i = 1; i < 4; i++)
      {
        // Only add non-zero items
        if(digitGroups[i] != 0)
        {
          // Build the string to add as a prefix
          string prefix = groupText[i] + " " + _scaleNumbers[i];

          if(combined.Length != 0)
            prefix += appendAnd ? "" : ", ";

          // Opportunity to add 'and' is ended
          appendAnd = false;

          // Add the three-digit group to the combined string
          combined = prefix + combined;
        }
      }

      // Negative rule
      if(number < 0)
        combined = "- " + combined;

      return combined;
    }



    // Converts a three-digit group into English words
    private string ThreeDigitGroupToWords(long threeDigits)
    {
      // Initialise the return text
      string groupText = "";

      // Determine the hundreds and the remainder
      long hundreds = threeDigits / 100;
      long tensUnits = threeDigits % 100;

      // Hundreds rules
      if(hundreds != 0)
      {
        groupText += _smallNumbers[hundreds] + " Hundred";

        if(tensUnits != 0)
          groupText += " ";
      }

      // Determine the tens and units
      long tens = tensUnits / 10;
      long units = tensUnits % 10;

      // Tens rules
      if(tens >= 2)
      {
        groupText += _tens[tens];
        if(units != 0)
          groupText += " " + _smallNumbers[units];
      }
      else if(tensUnits != 0)
        groupText += _smallNumbers[tensUnits];

      return groupText;
    }

    public bool isValidStudent(string userNameET, string passwordET)
    {

      string strClass = "isValidStudent";
      string strSequence = "";

      bool blnFlag = false;
      int result = 0;
      string strSql = "";
      SqlConnection Conn = new SqlConnection();
      DataSet dsStudents = new DataSet();

      string strUserId = null;
      userNameET = userNameET.Replace("'", "");
      passwordET = passwordET.Replace("'", "");

      try
      {
        if(userNameET.IndexOf("ve.student.educatablet.com") > 0)
        {
          strSql = "SELECT [AF_ID], [ST_PASSWORD]  FROM [IBANKING].[dbo].[IB_STUDENT]  WHERE  [ST_USERNAME] = '" + userNameET + "'  AND  [ST_PASSWORD] = '" + Encrypt(passwordET) + "'  ";
        }
        else
        {
          strSql = "SELECT [AF_ID], [ST_PASSWORD]  FROM [IBANKING].[dbo].[IB_TEACHER]  WHERE  [ST_USERNAME] = '" + userNameET + "'  AND  [ST_PASSWORD] = '" + Encrypt(passwordET) + "'  ";
        }
        Conn = CrearConexionPersonas();
        SqlDataAdapter daStudents = new SqlDataAdapter(strSql, Conn);
        daStudents.Fill(dsStudents, "DATA");

        result = dsStudents.Tables["DATA"].Rows.Count;
        if(result > 0)
        {
          return true;
        }
        //log.insertarLog(userNameET, strClass, "PASO 1", strSql.Replace("'", "") + result.ToString(), strSequence);
        return false;
      }
      catch(Exception ex)
      {
        //log.insertarLog(userNameET, strClass, "PASO 2", strSql.Replace("'", "") + ex.Message, strSequence);
        return false;
      }
      finally
      {
        cerrarConexion(Conn);

      }

    }


  }
}
