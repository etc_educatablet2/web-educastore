﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ETC.Structures.Models;
using WebEducaStore.Models;

namespace WebEducaStore.Controllers
{
    /// <summary>
    /// This class is intended to execute before calling any action of the 
    /// project. It verifiy if the user has the permissions to access and
    /// warranty the object person is loaded in session
    /// </summary>
    public class VerifyAuthFilterAttribute : ActionFilterAttribute
    {
        [Authorize]
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            PersonEducaDesk person;

            //load the person session object
            if (filterContext.HttpContext.Session["Person"] == null)
            {
                //load the person object for the session
                person = new PersonEducaDesk(PersonSession.jsonToObject(filterContext.HttpContext.User.Identity.Name));
                person.setCustomInfo();
                //adds the person object to sessions
                filterContext.HttpContext.Session.Add("Person", person);
            }
            else
            {
                /*
                 * Verify the user accessing is the same as the
                 * previous user. This prevents logging out from another application
                 * and logging in thru myaccount with another user, and because of
                 * maintanance of session for this application (it passes the authorize because the user
                 * was logged thru myaccount), if user types url for this,
                 * it will open with the user previously logged out because of the session.
                 * */
                //obtain the actually user in session
                person = new PersonEducaDesk((PersonSession)filterContext.HttpContext.Session["Person"]);
                //obtain the user accessing the app
                PersonSession personTemp = PersonSession.jsonToObject(filterContext.HttpContext.User.Identity.Name);
                //if they are not the same, kick out
                if (person.Email != personTemp.Email)
                {
                    filterContext.HttpContext.Session.RemoveAll();
                    filterContext.HttpContext.Session.Add("Person", personTemp);
                    //filterContext.HttpContext.Response.RedirectPermanent("/myaccount", true);
                }
            }

            if (person.roles.ContainsKey("EducaDesk"))
            {
                string[] roles = { "TEACHER", "STUDENT" };
                string[] rolesAsignados = person.roles["EducaDesk"].ToArray();

                if (!roles.Any(rolesAsignados.Contains))
                    logOut(filterContext);
            }
            else
                logOut(filterContext);

            base.OnActionExecuting(filterContext);
        }

        private static void logOut(ActionExecutingContext filterContext)
        {
            filterContext.HttpContext.Session.RemoveAll();
            filterContext.HttpContext.Session.Abandon();
            filterContext.HttpContext.Session.Clear();
            filterContext.HttpContext.Response.RedirectPermanent("/myaccount/login/logOut", true);
        }
    }
}
