﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using ETC.BL;
using ETC.BL.Prices;
using ETC.Structures.Models;
using ETC.Structures.Models.Products;
using WebEducaStore.Models;
using WebEducaStore.Services;
using WebEducaStore.ViewModels;

namespace WebEducaStore.Controllers
{
    public class ContentController : Controller
    {
        private WebEducaStoreEntities db = new WebEducaStoreEntities();

        //|
        // GET: /ContentVM/
        [Authorize]
        public ActionResult ListProducts(int idCatalog = 0)
        {
            PersonEducaDesk person = (PersonEducaDesk)Session["Person"];

            //Instantiate the classes for server data
            ServCatalog servCatalog = new ServCatalog(ref db);
            ServContent servContent = new ServContent(ref db);

            //Instantiate the viewmodel
            ListContentVM listContentVm = new ListContentVM();

            //load all the catalogs for the institution
            listContentVm.Catalogs = servCatalog.getCatalogsForVM(person.roleInstitutionId);
            if (idCatalog != 0)
            {
                //load all the contents to show for the selected catalog
                listContentVm.Contents = servContent.getContentsForCatalogForVM(idCatalog, Convert.ToInt32(person.Id), (long)person.StoreCountryId);
                listContentVm.SetFilteredCatalog(idCatalog);
            }
            else
            {
                //load all the contents to show (unfiltered)
                //comentado por cambio de personsession
                //contentVM.Contents = servContent.getContentsForVM(person.roleInstitutionId, Convert.ToInt32(person.Id));
                listContentVm.Contents = servContent.getContentsForVM(listContentVm.Catalogs.Select(w => w.id).ToList(), Convert.ToInt32(person.Id), (long)person.StoreCountryId);
                listContentVm.SetUnfilteredCatalog();
            }

            // get the pricing for each content
            foreach (ContentVM contentVm in listContentVm.Contents)
            {
                contentVm.pricing = new PriceBL().GetPricingFromIds(new List<decimal> { contentVm.priceId }).FirstOrDefault();
            }

            // as catalogs are already loaded, iterate for contents catalog and
            // put the name of the catalog
            foreach (IEnumerable<CatalogVM> catalogs in listContentVm.Contents.Select(w => w.Catalogs))
            {
                foreach (CatalogVM catalogVm in catalogs)
                {
                    var firstOrDefault = listContentVm.Catalogs.FirstOrDefault(w => w.id == catalogVm.id);
                    if (firstOrDefault != null)
                    {
                        catalogVm.name = firstOrDefault.name;
                    }
                }
            }

            //need the person information for ids
            listContentVm.person = person;

            return View(listContentVm);
        }

        //NOT IN USE
        //[HttpPost]
        public ActionResult Prueba(int id)
        {
            if (Request.IsAjaxRequest())
            {
                //añadir al carrito por ejemplo
                int i = id;
            }

            //Instantiate the classes for server data
            ServCatalog servCatalog = new ServCatalog(ref db);
            ServContent servContent = new ServContent(ref db);
            //Instantiate the viewmodel
            ListContentVM contentVM = new ListContentVM();
            //load all the catalogs for the institution
            contentVM.Catalogs = servCatalog.getCatalogsForVM(41);
            //load all the contents to show (unfiltered)
            //contentVM.Contents = servCatalog.getContentsForVM(41);
            contentVM.Contents = servContent.getContentsForCatalogForVM(23);
            contentVM.SetUnfilteredCatalog();

            return PartialView("_listProductItems", contentVM);
        }

        //
        // AJAX: /ContentVM/AddToCart/
        [Authorize]
        [HttpPost]
        public ActionResult AddToCart(int priceId = 0)
        {
            PersonSession person = (PersonSession)Session["Person"];

            if (Request.IsAjaxRequest())
            {
                PriceBL priceBL = new PriceBL();
                ProductBL productBL = new ProductBL();

                /*
                 * Load the prices information (product ID, amount and currency ID, etc)
                 * */
                //service data for models
                CartBL cartBL = new CartBL();

                List<decimal> pricesId = new List<decimal>();
                pricesId.Add(priceId);
                List<Pricing> pricings = priceBL.GetPricingFromIds(pricesId.ToList());
                //add items to cart
                var storefrontCart = ConfigurationManager.AppSettings["STOREFRONT_EDUCASTORE"];

                bool wasAdded = cartBL.AddItemsToCart(pricings, Convert.ToInt32(person.Id),
                                                      storefrontCart);

                //load the product to add
                var product = productBL.GetProductById(pricings.FirstOrDefault().Price.ProductId);

                /*
                 * Instantiate the viewmodel wich provides the object
                 * with the content to be replaced in the view
                 * will be passed thru json object
                 * */
                AjaxAddToCartVM result = new AjaxAddToCartVM();

                if (wasAdded)
                {
                    result.isAdded = true;
                    //result.image = "<a data-placement=\"bottom\" title=\"Ya ha sido añadido al carrito\"> <img src='/Images/addedtocart.png' /></a>";
                    result.image = "<a data-placement=\"bottom\" title=\"Ya ha sido añadido al carrito\"><img src='" + @Url.Content("~/Images/addedtocart.png") + "' /></a>";
                    //add the div alertMessage again because the replacewith erases it
                    //and it need to be replaced again in case another item is added to cart
                    result.alertMessage = "<div id=\"alertMessage\">"
                      + "<div class=\"" + ConfigurationManager.AppSettings["CLASS_ALERT_SUCCESS"] + "\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"
                        + "Se ha agregado '" + Server.HtmlEncode(product.Name) + "' al carrito de compras"
                      + "</div>"
                      + "</div>";
                }
                else
                {
                    result.isAdded = false;
                    result.alertMessage = "<div id=\"alertMessage\">"
                      + "<div class=\"" + ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"] + "\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"
                        + "No se ha podido agregar '" + product.Name + "' al carrito de compras, puede que ya se haya asignado."
                      + "</div>"
                      + "</div>";
                }
                return Json(result);
            }
            return HttpNotFound();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}