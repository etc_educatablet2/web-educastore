﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using ETC.BL;
using ETC.BL.Purchase;
using ETC.Structures.Models;
using Newtonsoft.Json;
using WebEducaStore.Models;
using WebEducaStore.Services;
using WebEducaStore.ViewModels;

namespace WebEducaStore.Controllers
{
    [Authorize]
    public class CartController : Controller
    {
        private WebEducaStoreEntities db = new WebEducaStoreEntities();

        //
        // GET: /Cart/
        [Authorize]
        public ActionResult Index(int userId = 0)
        {
            PersonSession person = (PersonSession)Session["Person"];

            //Instantiate the classes for server data
            CartBL cartBL = new CartBL();
            ServCart servCart = new ServCart(ref db);
            ServCatalog servCatalog = new ServCatalog(ref db);
            //Instantiate the viewmodel
            CartListVM cartVM = new CartListVM();

            /*
            * get all the products for the person carts
            * with all the pricing information related
            * (only check for the discount added with the product
            * to the cart and only assign if valid)
            * */
            List<CarthasProduct> carthasProducts = cartBL.GetCartsProductsWithPricingForPerson((long)person.Id, ConfigurationManager.AppSettings["STOREFRONT_EDUCASTORE"], ConfigurationManager.AppSettings["STOREFRONT_EDUCADESK"]);

            //load the cart viewmodel
            cartVM = servCart.getCartForVm(carthasProducts);

            //load all the catalogs for the institution
            //contentVM.Catalogs = servCatalog.getCatalogsForVM(person.roleInstitutionId);
            //var catalogs = servCatalog.getCatalogsForVM(43);

            //validate if it has items
            //load the contents if applies
            cartVM.Content = servCart.getCartContentsForVm(carthasProducts, Convert.ToInt32(person.Id));

            //load the colors for distinct catalogs id
            //cartVM.catalogColors = servCart.getDistinctCatalogColorsForVm(cartVM);

            //need the person information for ids
            cartVM.person = person;

            return View(cartVM);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EmptyCart(int idCart)
        {
            PersonSession person = (PersonSession)Session["Person"];

            CartBL cartBL = new CartBL();

            var storefront = ConfigurationManager.AppSettings["STOREFRONT_EDUCASTORE"];

            cartBL.EmptyCarts((int)person.Id, storefront);

            return RedirectToAction("Index", new { });
        }

        [Authorize]
        [HttpPost]
        public ActionResult CheckoutConfirmed(int idCart)
        {
            PersonSession person = (PersonSession)Session["Person"];

            //Instantiate the classes for server data
            ServCart servCart = new ServCart(ref db);
            ServCheckout servCheckout = new ServCheckout(ref db);
            CartBL cartBL = new CartBL();

            Utility utils = new Utility(ref db);

            //load the cart
            /*
            * get all the products for the person carts
            * with all the pricing information related
            * (only check for the discount added with the product
            * to the cart and only assign if valid)
            * */
            List<CarthasProduct> carthasProducts = cartBL.GetCartsProductsWithPricingForPerson((long)person.Id, ConfigurationManager.AppSettings["STOREFRONT_EDUCASTORE"], ConfigurationManager.AppSettings["STOREFRONT_EDUCADESK"]);

            List<ContentVM> products = servCart.getCartContentsForVm(carthasProducts, (int)person.Id);
            //var productsInOrders = purchaseBL.GetListIdProductsInOrders((int) person.Id);

            /*
             * Validate if items is already bought
             * */
            //if (!productsInOrders.Intersect(carthasProducts.Select(w=>w.ProductID).ToList()).Any())
            if (!products.Any(w => w.alreadyBought))
            {
                //Check each content and add to a specified list of content
                //depending of the price
                List<ContentVM> productsWithPrice = new List<ContentVM>();
                List<ContentVM> productsWithoutPrice = new List<ContentVM>();
                foreach (var product in products)
                {
                    if (product.pricing != null && product.pricing.FinalAmount == 0)
                    {
                        productsWithoutPrice.Add(product);
                    }
                    else
                    {
                        productsWithPrice.Add(product);
                    }
                }

                /*
                 * Call the web services to place the order
                 * If there are items with price and without price, place two different orders
                 * Otherwise, place one order
                 * */
                //items without price
                bool contentsWithoutPriceJson = false;
                if (productsWithoutPrice.Count != 0)
                {
                    //build the object for the post rest WS
                    SetUserETC userEtc = servCheckout.buildObjForWsNoPrice(person, productsWithoutPrice);

                    /*
                     * Call the web service and obtain the json result into
                     * the corresponding object
                     * */
                    string json = utils.postJsonWs(userEtc, ConfigurationManager.AppSettings["WS_HOST"] +
                        ConfigurationManager.AppSettings["WS_ETC_SETORDERBYUSER"]);

                    //string json = null;

                    if (!String.IsNullOrEmpty(json))
                    {
                        //deserialize the json returned into the response object
                        ResponseUser response = JsonConvert.DeserializeObject<ResponseUser>(json);
                        if (response.users.FirstOrDefault().errorcode == 0)
                        {
                            contentsWithoutPriceJson = true;
                        }
                    }
                }

                //items with price
                bool contentsWithPriceJson = false;
                if (productsWithPrice.Count != 0)
                {
                    //build the object for the post rest WS
                    SetUserETC userEtc = servCheckout.buildObjForWsWithPrice(person, productsWithPrice);

                    /*
                     * Call the web service and obtain the json result into
                     * the corresponding object
                     * */
                    string json = utils.postJsonWs(userEtc, ConfigurationManager.AppSettings["WS_HOST"] + 
                        ConfigurationManager.AppSettings["WS_STUDENTS_SETORDERUSER"]);

                    if (!String.IsNullOrEmpty(json))
                    {
                        //deserialize the json returned into the response object
                        ResponseUser response = JsonConvert.DeserializeObject<ResponseUser>(json);
                        if (response.users.FirstOrDefault().errorcode == 0)
                        {
                            contentsWithPriceJson = true;
                        }
                    }
                }

                /*
                 * If the orders were created successfuly (at least one of two possible),
                 * load checkout viewmodel, empty cart and render a new view
                 * */
                if (contentsWithPriceJson || contentsWithoutPriceJson)
                {
                    /*
                    * Load ViewModel to show acquired items
                    * */
                    //Instantiate the viewmodel
                    CheckoutVM checkoutVM = new CheckoutVM();
                    //set the corresponding contents to the VM
                    checkoutVM.contentWithoutPrice = productsWithoutPrice;
                    checkoutVM.contentWithPrice = productsWithPrice;
                    //set the statuses of contents
                    checkoutVM.contentWithPriceSuccess = contentsWithPriceJson;
                    checkoutVM.contentWithoutPriceSuccess = contentsWithoutPriceJson;

                    /*
                     * Empty Cart
                     * */
                    var storefront = ConfigurationManager.AppSettings["STOREFRONT_EDUCASTORE"];

                    cartBL.EmptyCarts((int)person.Id, storefront);

                    return View("CheckoutSummary", checkoutVM);
                }
                else
                {
                    /*
                     * Error creating order, pay, petition, etc
                     * */
                    //Instantiate the viewmodel
                    CartListVM cartVM = new CartListVM();

                    //load the cart viewmodel
                    cartVM = servCart.getCartForVm(carthasProducts);

                    //load all the catalogs for the institution
                    //contentVM.Catalogs = servCatalog.getCatalogsForVM(person.roleInstitutionId);
                    //var catalogs = servCatalog.getCatalogsForVM(43);

                    //validate if it has items
                    //load the contents if applies
                    cartVM.Content = servCart.getCartContentsForVm(carthasProducts, Convert.ToInt32(person.Id));

                    //put the error of content already bought
                    cartVM.errorMessage = "Ocurrió un problema al generar la orden, contacte al administrador.";

                    cartVM.person = person;

                    return View("Index", cartVM);
                }
            }
            else
            {
                //Instantiate the viewmodel
                CartListVM cartVM = new CartListVM();

                //load the cart viewmodel
                cartVM = servCart.getCartForVm(carthasProducts);

                //load all the catalogs for the institution
                //contentVM.Catalogs = servCatalog.getCatalogsForVM(person.roleInstitutionId);
                //var catalogs = servCatalog.getCatalogsForVM(43);

                //validate if it has items
                //load the contents if applies
                cartVM.Content = servCart.getCartContentsForVm(carthasProducts, Convert.ToInt32(person.Id));

                //put the error of content already bought
                cartVM.errorMessage = "Ya ha adquirido algún contenido de la lista";

                cartVM.person = person;

                return View("Index", cartVM);
            }
        }

        //
        // AJAX: /ContentVM/RemoveFromCart/
        [Authorize]
        [HttpPost]
        public ActionResult RemoveFromCart(int idProduct = 0, int idCart = 0)
        {
            PersonSession person = (PersonSession)Session["Person"];

            if (Request.IsAjaxRequest())
            {
                CartBL cartBL = new CartBL();
                ProductBL productBL = new ProductBL();
                ServCart servCart = new ServCart(ref db);

                //load the product to remove
                var product = productBL.GetProductById(idProduct);

                /*
                 * REMOVE FROM CART
                 * */

                /*
                 * Gets the cart id for the user.
                 * Creates it if it does not exist
                 * */
                var storefrontCart = ConfigurationManager.AppSettings["STOREFRONT_EDUCASTORE"];

                var wasRemoved = cartBL.RemoveFromCart(product.Id, (long)person.Id, storefrontCart, (long)person.StoreCountryId);

                /*
                 * Instantiate the viewmodel wich provides the object
                 * with the content to be replaced in the view
                 * will be passed thru json object
                 * */
                AjaxRemoveFromCart result = new AjaxRemoveFromCart();

                if (wasRemoved)
                {
                    result.wasRemoved = true;

                    //add the div alertMessage again because the replacewith erases it
                    //and it need to be replaced again in case another item is added to cart
                    result.alertMessage = "<div id=\"alertMessage\">"
                      + "<div class=\"" + ConfigurationManager.AppSettings["CLASS_ALERT_INFO"] + "\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"
                      + "Se ha eliminado '" + Server.HtmlEncode(product.Name) + "' del carrito de compras"
                      + "</div>"
                      + "</div>";

                    //load the cart
                    /*
                    * get all the products for the person carts
                    * with all the pricing information related
                    * (only check for the discount added with the product
                    * to the cart and only assign if valid)
                    * */
                    List<CarthasProduct> carthasProducts = cartBL.GetCartsProductsWithPricingForPerson((long)person.Id, ConfigurationManager.AppSettings["STOREFRONT_EDUCASTORE"], ConfigurationManager.AppSettings["STOREFRONT_EDUCADESK"]);

                    //load the cart viewmodel
                    result.cartListVm = servCart.getCartForVm(carthasProducts);
                }
                else
                {
                    result.wasRemoved = false;
                    result.alertMessage = "<div id=\"alertMessage\">"
                      + "<div class=\"" + ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"] + "\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"
                      + "No se ha podido eliminar '" + product.Name + "' del carrito de compras, puede que ya haya sido eliminado."
                      + "</div>"
                      + "</div>";
                }
                return Json(result);
            }
            return HttpNotFound();
        }
    }
}