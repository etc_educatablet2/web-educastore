﻿using System.Web.Mvc;

namespace WebEducaStore.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            //FormsAuthentication.SetAuthCookie("lpadilla@educatablet.com", false);
            //PersonSession person = new PersonSession();
            //person = person.getPerson("lpadilla@educatablet.com");
            //Session.Add("Person", person);

            return RedirectToAction("ListProducts", "Content");
        }

        public void LogOut()
        {
            HttpContext.Session.RemoveAll();
            HttpContext.Session.Abandon();
            HttpContext.Session.Clear();
            HttpContext.Response.RedirectPermanent("/myaccount/login/logOut", true);
        }
    }
}
