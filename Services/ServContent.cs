﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using ETC.BL;
using ETC.BL.Common;
using ETC.BL.Prices;
using ETC.BL.Purchase;
using WebEducaStore.Models;
using WebEducaStore.ViewModels;

namespace WebEducaStore.Services
{
    public class ServContent : PriceBL
    {
        private WebEducaStoreEntities db;

        public ServContent()
        {
        }

        public ServContent(ref WebEducaStoreEntities context)
        {
            this.db = context;
        }

        /// <summary>
        /// Get the contents list for an student and institution (ordered by title), this works for a viewmodel
        /// The idStudent is to check if the content has been already bought
        /// </summary>
        /// <param name="catalogIds"></param>
        /// <param name="idPerson"></param>
        /// <param name="idCountry"></param>
        /// <returns></returns>
        public List<ContentVM> getContentsForVM(List<decimal> catalogIds, long idPerson = 0, long idCountry = 0)
        {
            //gets all the content bought for the student
            PurchaseBL purchaseBl = new PurchaseBL();
            List<long> booksAlreadyBought = purchaseBl.GetListIdProductsInOrders((int)idPerson);

            List<decimal> booksAlreadyInCart =
                db.ET_CART.Where(w => w.ID_PERSON == idPerson).SelectMany(w => w.ET_CARTHASPRODUCT.Select(x => x.ID_PRODUCT)).ToList();

            DateTime date = DateTime.Now;

            var cover_url = ConfigurationManager.AppSettings["COVER_URL"];

            var storefront = ConfigurationManager.AppSettings["STOREFRONT_EDUCADESK"];
            var storefrontId = new BrandBL().GetIdByName(storefront);

            var status = CommonStatus.Active.ToString();

            var query = from pr in db.ET_PRODUCT
                        where pr.ET_CONTENT.ET_CATALOGHASCONTENT.Any(a => catalogIds.Contains(a.ID_CATALOG))
                            //&& pr.ET_SUBPRODUCTTYPE.ET_PRODUCTTYPE.PT_DESCRIPTION == "CONTENT" && !pr.ET_CONTENT.ET_DRMCONTENT.Any()
                        && pr.ET_SUBPRODUCTTYPE.ID_PRODUCTTYPE == 1
                            //&& !ct.ET_DRMCONTENT.Any()
                        && pr.ET_PRODUCTMARKET.Any(w => w.ET_PRICE.Any(price => price.ID_COUNTRY == idCountry && price.PR_STATUS == status && price.ID_BRAND == storefrontId
                            && (price.PR_STARTDATE == null || (date >= price.PR_STARTDATE && date <= price.PR_ENDDATE) || (date >= price.PR_STARTDATE && price.PR_ENDDATE == null))))
                        orderby pr.PR_NAME ascending
                        select new ContentVM()
                            {
                                image_path = cover_url + pr.ET_PRODUCTFILE.FirstOrDefault(w => w.ID_FILETYPE == 2).PF_NAME,
                                //id = ct.ID_PRODUCT,
                                title = pr.PR_NAME,
                                author = (pr.ET_CONTENT.ET_NATURALPERSON.FirstOrDefault().ET_PERSON.PE_LASTNAME) + ", " + (pr.ET_CONTENT.ET_NATURALPERSON.FirstOrDefault().ET_PERSON.PE_FIRSTNAME),
                                priceId = pr.ET_PRODUCTMARKET.FirstOrDefault(price => price.ID_COUNTRY == idCountry).ET_PRICE.FirstOrDefault(price => price.PR_PRICE == 0).ID_PRICE,
                                publisher = pr.ET_CONTENT.ET_PERSON.FirstOrDefault().PE_FULLNAME,
                                Catalogs = pr.ET_CONTENT.ET_CATALOGHASCONTENT.Where(h => catalogIds.Contains(h.ID_CATALOG))
                                .Select(h => new CatalogVM() { id = h.ID_CATALOG }),
                                isAddedToCart = booksAlreadyInCart.Contains(pr.ID_PRODUCT),
                                alreadyBought = booksAlreadyBought.Contains((long)pr.ID_PRODUCT),
                                //type = pr.ET_SUBPRODUCTTYPE.ET_PRODUCTTYPE.PT_DESCRIPTION
                            };

            return query.ToList();
        }

        /// <summary>
        /// Get the contents list for an catalogId (ordered by title), this works for a viewmodel.
        /// The idStudent is to check if the content has been already bought
        /// </summary>
        /// <param name="idInstitution"></param>
        /// <param name="idPerson"></param>
        /// <returns></returns>
        public List<ContentVM> getContentsForCatalogForVM(int catalogId, long idPerson = 0, long idCountry = 0)
        {
            //gets all the content bought for the student
            PurchaseBL purchaseBl = new PurchaseBL();
            List<long> booksAlreadyBought = purchaseBl.GetListIdProductsInOrders((int)idPerson);

            List<decimal> booksAlreadyInCart =
                db.ET_CART.Where(w => w.ID_PERSON == idPerson).SelectMany(w => w.ET_CARTHASPRODUCT.Select(x => x.ID_PRODUCT)).ToList();

            DateTime date = DateTime.Now;

            var cover_url = ConfigurationManager.AppSettings["COVER_URL"];

            var storefront = ConfigurationManager.AppSettings["STOREFRONT_EDUCADESK"];
            var storefrontId = new BrandBL().GetIdByName(storefront);

            var status = CommonStatus.Active.ToString();

            var query = from pr in db.ET_PRODUCT
                        where pr.ET_CONTENT.ET_CATALOGHASCONTENT.Any(a => a.ID_CATALOG == catalogId)
                            //&& ct.ET_SUBPRODUCTTYPE.ET_PRODUCTTYPE.PT_DESCRIPTION == "CONTENT" && !ct.ET_CONTENT.ET_DRMCONTENT.Any()
                        && pr.ET_SUBPRODUCTTYPE.ID_PRODUCTTYPE == 1
                            //&& !ct.ET_DRMCONTENT.Any()
                        && pr.ET_PRODUCTMARKET.Any(w => w.ET_PRICE.Any(price => price.ID_COUNTRY == idCountry && price.PR_STATUS == status && price.PR_STATUS == status && price.ID_BRAND == storefrontId
                            && (price.PR_STARTDATE == null || (date >= price.PR_STARTDATE && date <= price.PR_ENDDATE) || (date >= price.PR_STARTDATE && price.PR_ENDDATE == null))))
                        orderby pr.PR_NAME ascending
                        select new ContentVM()
                            {
                                image_path = cover_url + pr.ET_PRODUCTFILE.FirstOrDefault(w => w.ID_FILETYPE == 2).PF_NAME,
                                //id = pr.ID_PRODUCT,
                                title = pr.PR_NAME,
                                author = (pr.ET_CONTENT.ET_NATURALPERSON.FirstOrDefault().ET_PERSON.PE_LASTNAME) + ", " + (pr.ET_CONTENT.ET_NATURALPERSON.FirstOrDefault().ET_PERSON.PE_FIRSTNAME),
                                priceId = pr.ET_PRODUCTMARKET.FirstOrDefault(price => price.ID_COUNTRY == idCountry).ET_PRICE.FirstOrDefault(price => price.PR_PRICE == 0).ID_PRICE,
                                publisher = pr.ET_CONTENT.ET_PERSON.FirstOrDefault().PE_FULLNAME,
                                Catalogs = pr.ET_CONTENT.ET_CATALOGHASCONTENT.Where(h => h.ID_CATALOG == catalogId)
                                .Select(h => new CatalogVM() { id = h.ID_CATALOG }),
                                isAddedToCart = booksAlreadyInCart.Contains(pr.ID_PRODUCT),
                                alreadyBought = booksAlreadyBought.Contains((long)pr.ID_PRODUCT),
                                //type = pr.ET_SUBPRODUCTTYPE.ET_PRODUCTTYPE.PT_DESCRIPTION
                            };

            return query.ToList();
        }
    }
}