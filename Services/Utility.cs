﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using WebEducaStore.Models;

namespace WebEducaStore.Services
{
  public class Utility
  {
    private WebEducaStoreEntities db;

    public Utility()
    {
    }

    public Utility(ref WebEducaStoreEntities context)
    {
      this.db = context;
    }

    private string Key = "";
    private readonly byte[] IVector = new byte[8] { 27, 9, 45, 27, 0, 72, 171, 54 };

    public string Decrypt(string inputString)
    {
      byte[] buffer = Convert.FromBase64String(inputString);
      TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider();
      MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
      tripleDes.Key = MD5.ComputeHash(ASCIIEncoding.UTF8.GetBytes(Key));
      tripleDes.IV = IVector;
      ICryptoTransform ITransform = tripleDes.CreateDecryptor();
      return Encoding.UTF8.GetString(ITransform.TransformFinalBlock(buffer, 0, buffer.Length));
    }

    public string GetSHA1HashData(string value)
    {
      var data = Encoding.UTF8.GetBytes(value);
      var dataprueba = Encoding.UTF8.GetChars(data);
      var hashData = new SHA1Managed().ComputeHash(data);

      var hash = string.Empty;

      foreach(var b in hashData)
        hash += b.ToString("X2");

      return hash;
    }

    public string GetSha1Token(string strUser)
    {
      string strSHA1 = "";

      var query = from p in db.ET_PERSONHASTOKEN
                  where p.USERID == strUser
                  select p.TOKEN;
      strSHA1 = query.SingleOrDefault();
      if(!String.IsNullOrEmpty(strSHA1))
      {
        strSHA1 = Decrypt(strSHA1);
        return strSHA1;
      }
      return null;
    }

    /// <summary>
    /// Calls Rest POST Web Service, sending an object which is converted
    /// to json and then return the json from response or null if the 
    /// call was not OK
    /// </summary>
    /// <param name="obj">object that will be serialized into json to send</param>
    /// <param name="fullPath">the full url path to call the WS</param>
    /// <returns></returns>
    public string postJsonWs(object obj, string fullPath)
    {
      // Gets the url of the webservice
      string url = fullPath;

      // Create a request using a URL that can receive a post. 
      WebRequest request = WebRequest.Create(url);
      // Set the Method property of the request to POST.
      request.Method = "POST";
      // Convert the object into a json string
      //string p = "{\"users\":[]}";
      string json = JsonConvert.SerializeObject(obj);
      // Convert the POST data to byte array
      byte[] byteArray = Encoding.UTF8.GetBytes(json);
      // Set the ContentType property of the WebRequest.
      request.ContentType = "application/json";
      // Add headers that WS is requiring
      request.Headers.Add(HttpRequestHeader.AcceptCharset, "utf-8");
      request.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-US,en;q=0.8,es;q=0.6,gl;q=0.4");
      // Set the ContentLength property of the WebRequest.
      request.ContentLength = byteArray.Length;
      // Get the request stream.
      Stream dataStream = request.GetRequestStream();
      // Write the data to the request stream.
      dataStream.Write(byteArray, 0, byteArray.Length);
      // Close the Stream object.
      dataStream.Close();
      // Get the response.
      WebResponse response = request.GetResponse();

      string responseFromServer = null;

      if(((HttpWebResponse) response).StatusCode == HttpStatusCode.OK)
      {
        //// Display the status.
        //Console.WriteLine(((HttpWebResponse) response).StatusDescription);
        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        // Open the stream using a StreamReader for easy access.
        StreamReader reader = new StreamReader(dataStream);
        // Read the content.
        responseFromServer = reader.ReadToEnd();
        //close the response stream
        reader.Close();
      }

      // Clean up the streams.
      dataStream.Close();
      response.Close();

      return responseFromServer;
    }

    /// <summary>
    /// Calls Rest POST Web Service, sending an object which is converted
    /// to json and then return the json from response or null if the 
    /// call was not OK
    /// </summary>
    /// <param name="json">the raw json string to send to WS</param>
    /// <param name="fullPath">the full url path to call the WS</param>
    /// <returns></returns>
    public string postJsonWs(string json, string fullPath)
    {
      // Gets the url of the webservice
      string url = fullPath;

      // Create a request using a URL that can receive a post. 
      WebRequest request = WebRequest.Create(url);
      // Set the Method property of the request to POST.
      request.Method = "POST";
      // Convert the object into a json string
      //string p = "{\"users\":[]}";
      // Convert the POST data to byte array
      byte[] byteArray = Encoding.UTF8.GetBytes(json);
      // Set the ContentType property of the WebRequest.
      request.ContentType = "application/json";
      // Add headers that WS is requiring
      request.Headers.Add(HttpRequestHeader.AcceptCharset, "utf-8");
      request.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-US,en;q=0.8,es;q=0.6,gl;q=0.4");
      // Set the ContentLength property of the WebRequest.
      request.ContentLength = byteArray.Length;
      // Get the request stream.
      Stream dataStream = request.GetRequestStream();
      // Write the data to the request stream.
      dataStream.Write(byteArray, 0, byteArray.Length);
      // Close the Stream object.
      dataStream.Close();
      // Get the response.
      WebResponse response = request.GetResponse();

      string responseFromServer = null;

      if(((HttpWebResponse) response).StatusCode == HttpStatusCode.OK)
      {
        //// Display the status.
        //Console.WriteLine(((HttpWebResponse) response).StatusDescription);
        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        // Open the stream using a StreamReader for easy access.
        StreamReader reader = new StreamReader(dataStream);
        // Read the content.
        responseFromServer = reader.ReadToEnd();
        //close the response stream
        reader.Close();
      }

      // Clean up the streams.
      dataStream.Close();
      response.Close();

      return responseFromServer;
    }
  }
}