﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using ETC.BL;
using WebEducaStore.Models;
using WebEducaStore.ViewModels;

namespace WebEducaStore.Services
{
    public class ServCatalog
    {
        private WebEducaStoreEntities db;

        public ServCatalog()
        {
        }

        public ServCatalog(ref WebEducaStoreEntities context)
        {
            this.db = context;
        }

        /// <summary>
        /// Obtain a list of catalogs for an institution levels
        /// </summary>
        /// <param name="idInstitution">the id of institution</param>
        /// <returns></returns>
        public List<ET_CATALOG> getCatalogs(int idInstitution)
        {
            //return db.ET_CATALOG.Where(s => s.IB_LEVEL.Any(x => x.ET_INSTITUTION.idinstitution == idInstitution)).ToList();
            return db.ET_CATALOG.Where(s => s.ID_INSTITUTION == idInstitution).ToList();
        }

        /// <summary>
        /// Get the catalogs list for an institution, this works for a viewmodel
        /// </summary>
        /// <param name="idInstitution"></param>
        /// <returns></returns>
        public List<CatalogVM> getCatalogsForVM(int idInstitution)
        {
            var storefront = ConfigurationManager.AppSettings["STOREFRONT_EDUCASTORE"];
            var storefrontId = new BrandBL().GetIdByName(storefront);

            var query = from cat in db.ET_CATALOG
                        //where cat.IB_LEVEL.Any(x => x.ET_INSTITUTION.idinstitution == idInstitution) && cat.ID_BRAND == storefrontId
                        where cat.ID_INSTITUTION == idInstitution && cat.ET_BRAND.Any(w => w.ID_BRAND == storefrontId)
                        orderby cat.DESCRIPCION ascending
                        select new CatalogVM() { id = cat.ID_CATALOG, name = cat.DESCRIPCION };

            return query.ToList();
        }
    }
}