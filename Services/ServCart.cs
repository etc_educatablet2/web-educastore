﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using ETC.BL.Purchase;
using ETC.Structures.Models;
using WebEducaStore.Models;
using WebEducaStore.ViewModels;

namespace WebEducaStore.Services
{
    public class ServCart
    {
        private WebEducaStoreEntities db;

        public ServCart()
        {
        }

        public ServCart(ref WebEducaStoreEntities context)
        {
            this.db = context;
        }

        /// <summary>
        /// Get cart variables for the viewmodel, everyone but contents list
        /// </summary>
        /// <param name="idCart"></param>
        /// <returns>everything in CartListVm but the content list</returns>
        public CartListVM getCartForVm(List<CarthasProduct> carthasProducts)
        {
            CartListVM cartListVm = new CartListVM();
            if (carthasProducts.Count > 0)
            {
                cartListVm.cartId = (int)carthasProducts.FirstOrDefault().CartID;
                cartListVm.subtotal = carthasProducts.Sum(w => w.Product.Pricing.SubTotalAmount);
                cartListVm.tax = carthasProducts.Sum(w => w.Product.Pricing.TaxAmount);
                cartListVm.total = carthasProducts.Sum(w => w.Product.Pricing.FinalAmount);
                cartListVm.currency = carthasProducts.FirstOrDefault().Product.Pricing.CurrencyAbbreviation;
                cartListVm.numberItems = carthasProducts.Count();
            }

            return cartListVm;
        }

        /// <summary>
        /// Get cart variables for the viewmodel, everyone but contents list
        /// </summary>
        /// <param name="idCart"></param>
        /// <returns>everything in CartListVm but the content list</returns>
        public CartListVM getCartForVm(int idCart)
        {
            //var query = from car in db.ET_CART
            //            where car.ID_CART == idCart
            //            select new CartListVM { cartId = car.ID_CART, subtotal = car.CA_SUBTOTAL, tax = car.CA_TAX, total = car.CA_TOTAL, currency = "BsF", numberItems = car.ET_CARTHASPRODUCT.Count };
            return new CartListVM();
        }
        /*
         * BORRARRRRR
         * */
        /// <summary>
        /// Get cart variables for the viewmodel, everyone but contents list
        /// </summary>
        /// <param name="cart"></param>
        /// <returns>everything in CartListVm but the content list</returns>
        public CartListVM getCartForVm(ET_CART cart)
        {
            CartListVM cartVm = new CartListVM();
            cartVm.cartId = cart.ID_CART;
            //cartVm.subtotal = cart.CA_SUBTOTAL;
            //cartVm.tax = cart.CA_TAX;
            //cartVm.total = cart.CA_TOTAL;
            cartVm.currency = "BsF";
            cartVm.numberItems = cart.ET_CARTHASPRODUCT.Count;

            return cartVm;
        }

        /// <summary>
        /// Get the content list for a specific cart (doesnt validate if cart exist)
        /// The idPerson is to check if the content has been already bought
        /// </summary>
        /// <param name="idCart"></param>
        /// <param name="idPerson"></param>
        /// <returns></returns>
        public List<ContentVM> getCartContentsForVm(List<CarthasProduct> carthasProducts, int idPerson)
        {
            //gets all the content bought for the student
            PurchaseBL purchaseBl = new PurchaseBL();
            List<long> booksAlreadyBought = purchaseBl.GetListIdProductsInOrders(idPerson);

            var cover_url = ConfigurationManager.AppSettings["COVER_URL"];

            List<ContentVM> contentVms = new List<ContentVM>();

            foreach (CarthasProduct carthasProduct in carthasProducts)
            {
                CarthasProduct product = carthasProduct;
                var query = from pr in db.ET_PRODUCT
                            where pr.ID_PRODUCT == product.ProductID
                            select new ContentVM()
                                {
                                    image_path = cover_url + pr.ET_PRODUCTFILE.FirstOrDefault(w => w.ID_FILETYPE == 2).PF_NAME,
                                    id = product.ProductID,
                                    title = pr.PR_NAME,
                                    author = (pr.ET_CONTENT.ET_NATURALPERSON.FirstOrDefault().ET_PERSON.PE_LASTNAME) + ", " + (pr.ET_CONTENT.ET_NATURALPERSON.FirstOrDefault().ET_PERSON.PE_FIRSTNAME),
                                    priceId = product.PriceID,
                                    publisher = pr.ET_CONTENT.ET_PERSON.FirstOrDefault().PE_FULLNAME,
                                    quantity = product.Quantity,
                                    alreadyBought = booksAlreadyBought.Contains(product.ProductID),
                                    //Catalogs = pr.ET_CONTENT.ET_CATALOGHASCONTENT.Where(h => catalogIds.Contains(h.ID_CATALOG))
                                    //.Select(h => new CatalogVM() { id = h.ID_CATALOG, name = h.ET_CATALOG.DESCRIPCION }),
                                    //type = pr.ET_SUBPRODUCTTYPE.ET_PRODUCTTYPE.PT_DESCRIPTION
                                };
                var contentVm = query.FirstOrDefault();
                contentVm.pricing = product.Product.Pricing;
                contentVms.Add(contentVm);
            }
            return contentVms;
        }

        /// <summary>
        /// Get the content list for a specific cart (doesnt validate if cart exist)
        /// The idPerson is to check if the content has been already bought
        /// </summary>
        /// <param name="idCart"></param>
        /// <param name="idPerson"></param>
        /// <returns></returns>
        public List<ContentVM> getCartContentsForVm(int idCart, int idPerson = 0)
        {
            //var query = from cont in db.IB_BOOK
            //            where cont.ET_CARTHASCONTENT.Any(x => x.ET_CART.id_cart == idCart)
            //            select new ContentVM() { id = cont.BO_ID, title = cont.BO_TITLE, author = cont.BO_AUTHOR1, publisher = cont.BO_PUBLISHER, Catalogs = cont.ET_CATALOGHASCONTENT.Select(h => h.ET_CATALOG) };

            //gets all the content bought for the person
            List<decimal> booksAlreadyBought = db.ET_SELLPRODUCTDETAIL.Where(a => a.ET_SELLPRODUCT.ID_PERSON == idPerson && a.ID_PRODUCT != 0).Select(b => b.ID_PRODUCT).ToList();

            //FUNCIONABA ANTES DEL CAMBIO DE IB_BOOK A ET_PRODUCT
            //var query = from cont in db.ET_CARTHASCONTENT
            //            where cont.ID_CART == idCart
            //            select new ContentVM() { id = cont.IB_BOOK.BO_ID, title = cont.IB_BOOK.BO_TITLE, author = cont.IB_BOOK.BO_AUTHOR1, publisher = cont.IB_BOOK.BO_PUBLISHER, unitprice = cont.unitprice, quantity = cont.quantity, Catalogs = cont.IB_BOOK.ET_CATALOGHASCONTENT.Select(h => h.ET_CATALOG), alreadyBought = booksAlreadyBought.Contains(cont.IB_BOOK.BO_ID) };

            var query = from cc in db.ET_CARTHASPRODUCT
                        where cc.ID_CART == idCart
                        select new ContentVM()
                        {
                            id = cc.ET_PRODUCT.ID_PRODUCT,
                            title = cc.ET_PRODUCT.PR_NAME,
                            author = (cc.ET_PRODUCT.ET_CONTENT.ET_NATURALPERSON.FirstOrDefault().ET_PERSON.PE_LASTNAME) + ", " + (cc.ET_PRODUCT.ET_CONTENT.ET_NATURALPERSON.FirstOrDefault().ET_PERSON.PE_FIRSTNAME),
                            publisher = cc.ET_PRODUCT.ET_CONTENT.ET_PERSON.FirstOrDefault().PE_FULLNAME,
                            //unitprice = cc.CD_UNITPRICE,
                            quantity = cc.CD_QUANTITY,
                            //Catalogs = cc.ET_PRODUCT.ET_CONTENT.ET_CATALOGHASCONTENT.Select(h => h.ET_CATALOG),
                            alreadyBought = booksAlreadyBought.Contains(cc.ID_PRODUCT),
                            type = cc.ET_PRODUCT.ET_SUBPRODUCTTYPE.ET_PRODUCTTYPE.PT_DESCRIPTION
                        };

            return query.ToList();
        }

        /// <summary>
        /// Returns the cart for the person if exists. if not, it creates it and gets it
        /// </summary>
        /// <param name="idPerson">the person id</param>
        /// <returns></returns>
        public int getCartId(int idPerson)
        {
            //find the cart for a user id
            ET_CART cart = db.ET_CART.SingleOrDefault(c => c.ID_PERSON == idPerson);

            /*
             * If there is not cart for the user, creates one and returns the id.
             * Otherwise, return the id of the existing cart
             * */
            if (cart == null)
            {
                //TODO BUSCAR EL USER, SU PAIS Y DE AHI COLOCAR EL CURRENCY CORRESPONDIENTE AL CARRITO

                ET_CART newCart = new ET_CART();
                newCart.CREATED_AT = DateTime.Now;
                //newCart.CA_SUBTOTAL = 0;
                //newCart.CA_TOTAL = 0;
                newCart.ID_PERSON = idPerson;
                newCart.ID_CURRENCY = 1;
                db.ET_CART.Add(newCart);
                db.SaveChanges();

                return newCart.ID_CART;
            }
            else
            {
                return cart.ID_CART;
            }
        }

        /// <summary>
        /// Add a content to a cart
        /// </summary>
        /// <param name="
        /// ent"></param>
        /// <param name="idCart"></param>
        /// <returns></returns>
        public bool addItemToCart(int idProduct, int idCart)
        {
            ET_CART cart = db.ET_CART.SingleOrDefault(x => x.ID_CART == idCart);

            //check if the content is already in the cart
            bool contentExist = db.ET_CARTHASPRODUCT.Where(w => w.ID_CART == idCart && w.ID_PRODUCT == idProduct).Any();

            ////gets the price of the content
            //decimal? price = db.IB_BOOK.SingleOrDefault(w => w.BO_ID == idContent).BO_AMOUNTET;

            if (!contentExist && cart != null)
            {
                try
                {
                    //creates a new row to assign item to cart
                    ET_CARTHASPRODUCT cartHasContent = new ET_CARTHASPRODUCT();
                    cartHasContent.CD_QUANTITY = 1;
                    //cartHasContent.unitprice = (price != null) ? Convert.ToDouble(price) : 0;
                    cartHasContent.CD_UNITPRICE = 0;
                    cartHasContent.ID_PRODUCT = idProduct;
                    cartHasContent.ID_CART = cart.ID_CART;

                    db.ET_CARTHASPRODUCT.Add(cartHasContent);

                    //updates the cart information
                    //cart.CA_SUBTOTAL = cart.CA_SUBTOTAL + cartHasContent.CD_UNITPRICE;
                    //cart.CA_TOTAL = cart.CA_TOTAL + cartHasContent.CD_UNITPRICE;
                    db.Entry(cart).State = EntityState.Modified;

                    db.SaveChanges();

                    return true;
                }
                catch (DataException)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Removes an item from cart, validating that both exist. Also updates amounts
        /// </summary>
        /// <param name="idContent"></param>
        /// <param name="idCart"></param>
        /// <returns></returns>
        public bool removeItemFromCart(int idProduct = 0, int idCart = 0)
        {
            ET_CART cart = db.ET_CART.SingleOrDefault(x => x.ID_CART == idCart);

            //check if the content is already in the cart
            ET_CARTHASPRODUCT contentInCart = db.ET_CARTHASPRODUCT.SingleOrDefault(w => w.ID_CART == idCart && w.ID_PRODUCT == idProduct);

            if (cart != null && contentInCart != null)
            {
                try
                {
                    //store important vars before removing the row
                    double price = contentInCart.CD_UNITPRICE;

                    //deletes the row
                    db.Entry(contentInCart).State = EntityState.Deleted;

                    //updates the cart
                    //TODO actualizar los demas datos del carrito al eliminar item
                    //cart.CA_SUBTOTAL = cart.CA_SUBTOTAL - price;
                    //cart.CA_TOTAL = cart.CA_TOTAL - price;
                    db.Entry(cart).State = EntityState.Modified;

                    db.SaveChanges();

                    return true;
                }
                catch (DataException)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Empty the cart items and updates its amounts
        /// </summary>
        /// <param name="cart"></param>
        /// <returns></returns>
        public bool emptyCart(ET_CART cart)
        {
            if (cart.ET_CARTHASPRODUCT.Count > 0)
            {
                try
                {
                    //updates car amounts
                    //cart.CA_TOTAL = 0;
                    //cart.CA_TAX = 0;
                    //cart.CA_SUBTOTAL = 0;
                    db.Entry(cart).State = EntityState.Modified;

                    //removes contents from cart
                    db.ET_CARTHASPRODUCT.Where(x => x.ID_CART == cart.ID_CART).ToList().ForEach(s => db.ET_CARTHASPRODUCT.Remove(s));

                    //saves all pending changes
                    db.SaveChanges();

                    return true;
                }
                catch (DataException)
                {
                    return false;
                }
            }
            else
            {
                return true;
            }


        }

        /// <summary>
        /// Checks if there is any item in the cart
        /// </summary>
        /// <param name="idCart"></param>
        /// <returns></returns>
        public bool cartHasItems(int idCart = 0)
        {
            //check if the cart has any content
            return db.ET_CART.Where(x => x.ID_CART == idCart).SingleOrDefault().ET_CARTHASPRODUCT.Any();
        }

        /// <summary>
        /// Gets the structure for colors for the distinct catalogs id
        /// loaded in the viewmodel
        /// </summary>
        /// <param name="cartVM"></param>
        /// <returns></returns>
        public Dictionary<decimal, string> getDistinctCatalogColorsForVm(CartListVM cartVM)
        {
            //structure to store colors for catalogs
            Dictionary<decimal, string> catalogColors = new Dictionary<decimal, string>();
            //var to control colors
            int countColor = 0;
            int totalColors = cartVM.totalColors();
            /*
             * Adds to a key-value the colors for distincts catalogs ids
             * contained in contents
             * */
            foreach (var content in cartVM.Content)
            {
                foreach (var catalog in content.Catalogs)
                {
                    //if the catalog id is not already inside the dictionary
                    if (!catalogColors.ContainsKey(catalog.id))
                    {
                        //adds the catalog id and a color
                        catalogColors.Add(catalog.id, cartVM.Colors[countColor]);
                        countColor++;
                        //if colors are over, reset the color counter
                        if (countColor == (totalColors - 1))
                        {
                            countColor = 0;
                        }
                    }
                }
            }
            return catalogColors;
        }

        /// <summary>
        /// Checks if any item of the cart had been already bought in
        /// a previous order
        /// </summary>
        /// <param name="cart"></param>
        /// <returns></returns>
        public bool contentsHasBeenBought(ET_CART cart)
        {
            decimal[] productId = cart.ET_CARTHASPRODUCT.Select(w => w.ET_PRODUCT.ID_PRODUCT).ToArray();

            return db.ET_SELLPRODUCTDETAIL.Where(x => x.ET_SELLPRODUCT.ET_NATURALPERSON.ID_PERSON == cart.ID_PERSON).Any(x => productId.Contains(x.ID_PRODUCT));
        }

    }
}