﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ETC.Structures.Models;
using WebEducaStore.Controllers;
using WebEducaStore.Models;
using WebEducaStore.ViewModels;

namespace WebEducaStore.Services
{
    public class ServCheckout
    {
        private WebEducaStoreEntities db;
        Funciones objFunc = new Funciones();


        public ServCheckout()
        {
        }

        public ServCheckout(ref WebEducaStoreEntities context)
        {
            this.db = context;
        }

        /// <summary>
        /// Return the plain password from an user
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string getPasswordForToken(string email)
        {
            string password = "";

            var query = from pw in db.IB_PASSWORD
                        where pw.UP_ID == email
                        select pw.UP_PASSWORD;

            Utility utils = new Utility();
            password = utils.Decrypt(query.SingleOrDefault());

            return password;
        }

        /// <summary>
        /// Return the idcountry of a person
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public decimal getConfiguredUserCountry(string email)
        {
            var query = from pe in db.ET_PERSON
                        where pe.PE_ADDRESS == email
                        select pe.ID_COUNTRY;

            return query.SingleOrDefault();
        }

        /// <summary>
        /// Build the object according to the web service with products
        /// without price, placing the order with a payment of zero
        /// </summary>
        /// <param name="person">perssonsession viewmodel</param>
        /// <param name="cart">cart entity</param>
        /// <param name="contentsWithoutPrice">List of free contents</param>
        /// <returns></returns>
        public SetUserETC buildObjForWsNoPrice(PersonSession person, List<ContentVM> contentsWithoutPrice)
        {
            Utility utils = new Utility(ref db);

            //Instantiate the classes for server data
            ServCheckout servCheckout = new ServCheckout(ref db);

            //declare the root object
            SetUserETC userEtc = new SetUserETC();

            //declare and sets the empty list of userDTO to the root objects
            //it will only contain one item added later as it is one user placing the order
            userEtc.users = new List<SetUserDTO>();
            SetUserDTO userDto = new SetUserDTO();
            userDto.email = person.Email;
            //get the plain password
            string password = servCheckout.getPasswordForToken(person.Email);
            userDto.password = password;
            string clientUser = ConfigurationManager.AppSettings["ETC_SEED_EDUCADESK"];
            //armo el token
            string token = person.Email + password + person.FirstName + person.SecondName + person.LastName +
              person.SecondLastName + clientUser + utils.GetSha1Token(clientUser);
            userDto.token = utils.GetSHA1HashData(token);
            userDto.firstname = person.FirstName;
            userDto.secondname = person.SecondName;
            userDto.lastname = person.LastName;
            userDto.maidenname = person.SecondLastName;

            SetUserPhoneETC phone = new SetUserPhoneETC();
            phone.areacode = 0;
            phone.countrycode = 0;
            phone.phonenumber = 0;
            userDto.phone = phone;

            //get the id country
            decimal idcountry = servCheckout.getConfiguredUserCountry(person.Email);
            userDto.idcountry = Convert.ToInt32(idcountry);
            userDto.userid = clientUser;

            //initialize the invoice list
            userDto.invoice = new List<SetSellOrderDTO>();
            //initialize the unique invoice item and populate
            SetSellOrderDTO sellOrderDto = new SetSellOrderDTO();
            string currentDate = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            sellOrderDto.invoicedate = currentDate;
            sellOrderDto.invoicetotalamount = 0;
            sellOrderDto.invoicecurrency = contentsWithoutPrice.FirstOrDefault().pricing.CurrencyAbbreviation;

            //initialize the list of details for the invoice
            sellOrderDto.detail = new List<SetSellOrderDetailDTO>();
            /*
             * For each product, add a detail object to the list
             * */
            foreach (var product in contentsWithoutPrice)
            {
                //initialize the detail of product
                SetSellOrderDetailDTO invoiceProduct = new SetSellOrderDetailDTO();
                long productId = Convert.ToInt64(product.id);
                invoiceProduct.invoiceproduct = productId;
                invoiceProduct.invoiceproductprice = 0;
                invoiceProduct.invoiceproductquantity = product.quantity;
                //add the product details to the detail list of the invoice order
                sellOrderDto.detail.Add(invoiceProduct);
            }

            /*
             * Add a zero payment because this content is without price
             * */
            //initialize the list of payment for the invoice
            sellOrderDto.payment = new List<SetSellOrderPaymentDTO>();
            //initialize the unique payment for all free products
            SetSellOrderPaymentDTO sellOrderPaymentDto = new SetSellOrderPaymentDTO();
            sellOrderPaymentDto.paymentamount = 0;
            sellOrderPaymentDto.paymentdate = currentDate;
            //set the payment type to account deposit
            sellOrderPaymentDto.paymenttype = 3;
            sellOrderPaymentDto.paymentprocessor = ConfigurationManager.AppSettings["PAYMENTPROCESSOR_RESTWS"];
            sellOrderPaymentDto.paymentcurrency = contentsWithoutPrice.FirstOrDefault().pricing.CurrencyAbbreviation;
            sellOrderPaymentDto.paymentauthnumber = ConfigurationManager.AppSettings["PAYMENTAUTHNUM_RESTWS"];

            //add the unique payment item to the order detail
            sellOrderDto.payment.Add(sellOrderPaymentDto);
            //add the unique invoice item to the list of invoices
            userDto.invoice.Add(sellOrderDto);
            //add the only one user to the root list object
            userEtc.users.Add(userDto);

            return userEtc;
        }

        /// <summary>
        /// Build the object according to the web service with products
        /// with price, placing the order with no payment
        /// </summary>
        /// <param name="person">perssonsession viewmodel</param>
        /// <param name="cart">cart entity</param>
        /// <param name="contentsWithPrice">List of contents with price</param>
        /// <returns></returns>
        public SetUserETC buildObjForWsWithPrice(PersonSession person, List<ContentVM> contentsWithPrice)
        {
            Utility utils = new Utility(ref db);

            //Instantiate the classes for server data
            ServCheckout servCheckout = new ServCheckout(ref db);

            //declare the root object
            SetUserETC userEtc = new SetUserETC();

            //declare and sets the empty list of userDTO to the root objects
            //it will only contain one item added later as it is one user placing the order
            userEtc.users = new List<SetUserDTO>();
            SetUserDTO userDto = new SetUserDTO();
            userDto.email = person.Email;
            //get the plain password
            string password = servCheckout.getPasswordForToken(person.Email);
            userDto.password = password;
            string clientUser = ConfigurationManager.AppSettings["ETC_SEED_EDUCADESK"];
            //armo el token
            string token = person.Email + password + person.FirstName + person.SecondName + person.LastName +
              person.SecondLastName + clientUser + utils.GetSha1Token(clientUser);
            userDto.token = utils.GetSHA1HashData(token);
            userDto.firstname = person.FirstName;
            userDto.secondname = person.SecondName;
            userDto.lastname = person.LastName;
            userDto.maidenname = person.SecondLastName;

            SetUserPhoneETC phone = new SetUserPhoneETC();
            phone.areacode = 0;
            phone.countrycode = 0;
            phone.phonenumber = 0;
            userDto.phone = phone;

            //get the id country
            decimal idcountry = servCheckout.getConfiguredUserCountry(person.Email);
            userDto.idcountry = (long)Convert.ToInt32(idcountry);
            userDto.userid = clientUser;

            //initialize the invoice list
            userDto.invoice = new List<SetSellOrderDTO>();
            //initialize the unique invoice item and populate
            SetSellOrderDTO sellOrderDto = new SetSellOrderDTO();
            string currentDate = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            sellOrderDto.invoicedate = currentDate;
            sellOrderDto.invoicetotalamount = Convert.ToDecimal(contentsWithPrice.Sum(w=>w.pricing.FinalAmount));
            sellOrderDto.invoicecurrency = contentsWithPrice.FirstOrDefault().pricing.CurrencyAbbreviation;

            //initialize the list of details for the invoice
            sellOrderDto.detail = new List<SetSellOrderDetailDTO>();
            /*
             * For each product, add a detail object to the list
             * */
            foreach (var product in contentsWithPrice)
            {
                //initialize the detail of product
                SetSellOrderDetailDTO invoiceProduct = new SetSellOrderDetailDTO();
                long productId = Convert.ToInt64(product.id);
                invoiceProduct.invoiceproduct = productId;
                //put the total amount of the cart to the invoice
                invoiceProduct.invoiceproductprice = Convert.ToDecimal(product.pricing.FinalAmount);
                invoiceProduct.invoiceproductquantity = product.quantity;
                //add the product details to the detail list of the invoice order
                sellOrderDto.detail.Add(invoiceProduct);
            }

            //add the unique invoice item to the list of invoices
            userDto.invoice.Add(sellOrderDto);
            //add the only one user to the root list object
            userEtc.users.Add(userDto);

            return userEtc;
        }

    }

}