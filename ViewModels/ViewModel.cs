﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using ETC.Structures.Models;
using ETC.Structures.Models.Products;
using WebEducaStore.Models;

namespace WebEducaStore.ViewModels
{

    public class ListContentVM
    {
        public readonly string[] Colors = { "#005b27", "#854399", "#c7ba00", "#808284", "#389239", "#f16524", "#265895", "#9e246f", "#911f10", "#23338a", "#00ae8d", "#0098a9", "#6988af", "#3f793a", "#ee3e2c", "#008f3c" };
        public List<ContentVM> Contents { get; set; }
        public List<CatalogVM> Catalogs { get; set; }
        public bool isFilteredByCatalog;
        public CatalogVM FilteredCatalog { get; set; }
        public PersonSession person { get; set; }

        public ListContentVM()
        {
            this.Contents = new List<ContentVM>();
            this.Catalogs = new List<CatalogVM>();
            this.isFilteredByCatalog = false;
        }

        public void SetFilteredCatalog(int id)
        {
            this.isFilteredByCatalog = true;
            this.FilteredCatalog = new CatalogVM() { id = id, color = "lightgrey" };
        }

        public void SetUnfilteredCatalog()
        {
            this.isFilteredByCatalog = false;
            this.FilteredCatalog = null;
        }

        /// <summary>
        /// Get the total colors defined for catalogs
        /// </summary>
        /// <returns></returns>
        public int totalColors()
        {
            return Colors.Count();
        }
    }

    /// <summary>
    /// Clase to only store only the id and name of catalogs for this viewmodel
    /// </summary>
    public class CatalogVM
    {
        public decimal id;
        public string name;
        public string color { get; set; }

        public CatalogVM()
        {
        }
    }

    /// <summary>
    /// Class to only store only the id and name of catalogs for many viewmodels
    /// </summary>
    public class ContentVM
    {
        public decimal id;
        public string title;
        public string author;
        public string publisher;
        public string image_path;
        public string isbn;
        //only for listproducts
        public bool isAddedToCart;
        //only for cart
        public int quantity;
        //price for the content added to cart
        //pricing of the content
        public decimal priceId;
        public Pricing pricing;
        public IEnumerable<CatalogVM> Catalogs;
        //only for listproducts
        public bool alreadyBought;
        //type of product (ie. tablet, license, book)
        public string type;
    }

    public class CartListVM
    {
        public readonly string[] Colors = { "#005b27", "#854399", "#c7ba00", "#808284", "#389239", "#f16524", "#265895", "#9e246f", "#911f10", "#23338a", "#00ae8d", "#0098a9", "#6988af", "#3f793a", "#ee3e2c", "#008f3c" };
        public int cartId { get; set; }
        public string currency { get; set; }
        public double subtotal { get; set; }
        public Nullable<double> tax { get; set; }
        public double total { get; set; }
        public List<ContentVM> Content { get; set; }
        public Dictionary<decimal, string> catalogColors = new Dictionary<decimal, string>();
        public int numberItems { get; set; }
        public string errorMessage { get; set; }
        public string errorClass { get; set; }
        public PersonSession person { get; set; }

        public CartListVM()
        {
            this.errorClass = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
        }

        /// <summary>
        /// Get the total colors defined for catalogs
        /// </summary>
        /// <returns></returns>
        public int totalColors()
        {
            return Colors.Count();
        }
    }

    public class CheckoutVM
    {
        public List<ContentVM> contentWithPrice { get; set; }
        public List<ContentVM> contentWithoutPrice { get; set; }

        public bool contentWithPriceSuccess { get; set; }
        public bool contentWithoutPriceSuccess { get; set; }
    }

    /// <summary>
    /// Class to store some data for the AJAX add to cart function of listproduct action
    /// </summary>
    public class AjaxAddToCartVM
    {
        public bool isAdded { get; set; }
        //the image to be replaced when clicked (full html component)
        public string image { get; set; }
        //the message alert to be replaced when addtocart button clicked (full html component)
        public string alertMessage { get; set; }
    }

    /// <summary>
    /// Class to store some data for the AJAX remove from cart function of cart RemoveFromCart action
    /// </summary>
    public class AjaxRemoveFromCart
    {
        public bool wasRemoved { get; set; }
        //the message alert to be replaced when addtocart button clicked (full html component)
        public string alertMessage { get; set; }
        //the viewmodel with information about the cart
        public CartListVM cartListVm { get; set; }
    }

    #region objects for WS register order

    public class ResponseUser
    {
        public List<ResponseUserDTO> users { get; set; }
    }

    public class ResponseUserDTO
    {

        public string email { get; set; }

        public string token { get; set; }

        public long errorcode { get; set; }

        public IEnumerable<ResponseUserErrorMessageDTO> errormessage { get; set; }

        public ResponseUserDTO()
        {

        }
    }

    public class ResponseUserErrorMessageDTO
    {
        public string parameter { get; set; }

        public string message { get; set; }

        public ResponseUserErrorMessageDTO()
        {

        }
    }


    public class SetUserETC
    {
        public List<SetUserDTO> users { get; set; }
    }

    public class SetUserDTO
    {
        public string email { get; set; }

        public string password { get; set; }

        public string device { get; set; }

        public string token { get; set; }

        public string userid { get; set; }

        public string firstname { get; set; }

        public string secondname { get; set; }

        public string lastname { get; set; }

        public string maidenname { get; set; }

        public long idcountry { get; set; }

        public long institution { get; set; }

        public SetUserPhoneETC phone { get; set; }

        public List<long> level { get; set; }

        public List<SetSellOrderDTO> invoice { get; set; }

        public SetUserDTO()
        {

        }
    }

    public class SetSellOrderDTO
    {
        public string invoicedate { get; set; }

        public decimal invoicetotalamount { get; set; }

        public string invoicecurrency { get; set; }

        public List<SetSellOrderDetailDTO> detail { get; set; }

        public List<SetSellOrderPaymentDTO> payment { get; set; }

        public SetSellOrderDTO()
        {

        }
    }

    public class SetSellOrderDetailDTO
    {
        public long invoiceproduct { get; set; }

        public decimal invoiceproductprice { get; set; }

        public int invoiceproductquantity { get; set; }



        public SetSellOrderDetailDTO()
        {

        }
    }

    public class SetSellOrderPaymentDTO
    {
        public string paymentauthnumber { get; set; }

        public int paymenttype { get; set; }

        public string paymentcurrency { get; set; }

        public decimal paymentamount { get; set; }

        public string paymentdate { get; set; }

        public string paymentprocessor { get; set; }

        public SetSellOrderPaymentDTO()
        {

        }
    }

    public class SetUserPhoneETC
    {

        public long countrycode { get; set; }

        public long areacode { get; set; }

        public long phonenumber { get; set; }
    }

    #endregion
}