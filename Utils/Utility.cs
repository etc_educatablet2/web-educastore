﻿using System.Drawing;
using System.Net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace WebEducaStore.Utils
{
    public static class Utility
    {
        public static void InitializeLogger()
        {
            LogWriterFactory logWriterFactory = new LogWriterFactory();
            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
            Logger.SetLogWriter(logWriterFactory.Create());
        }

        public static bool VerifyImage(string filename)
        {
            if (string.IsNullOrEmpty(filename))
                return false;

            HttpWebResponse response = null;
            var request = (HttpWebRequest)WebRequest.Create(filename);
            request.Method = "HEAD";

            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                /* A WebException will be thrown if the status of the response is not `200 OK` */
                return false;
            }
            finally
            {
                // Close response
                if (response != null)
                {
                    response.Close();
                }
            }
            return true;
        }
    }
}