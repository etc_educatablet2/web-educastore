//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ET_PRODUCTMARKET
    {
        public ET_PRODUCTMARKET()
        {
            this.ET_PRICE = new HashSet<ET_PRICE>();
        }
    
        public decimal ID_PRODUCT { get; set; }
        public decimal ID_COUNTRY { get; set; }
        public string PM_STATUS { get; set; }
        public Nullable<System.DateTime> CREATED_AT { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_AT { get; set; }
        public string UPDATED_BY { get; set; }
    
        public virtual ET_COUNTRY ET_COUNTRY { get; set; }
        public virtual ICollection<ET_PRICE> ET_PRICE { get; set; }
        public virtual ET_PRODUCT ET_PRODUCT { get; set; }
    }
}
