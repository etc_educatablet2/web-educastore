//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class IB_EXECUTIVE
    {
        public string EX_COD { get; set; }
        public string EX_NAME { get; set; }
        public string EX_PHONE { get; set; }
        public string EX_EMAIL { get; set; }
    }
}
