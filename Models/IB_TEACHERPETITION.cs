//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class IB_TEACHERPETITION
    {
        public decimal SP_IDTEACHERPETITION { get; set; }
        public Nullable<decimal> ID_TEACHER { get; set; }
        public Nullable<decimal> ID_CONTENT { get; set; }
        public Nullable<System.DateTime> SP_EXPIRATION_DATE { get; set; }
        public Nullable<int> SP_MAX_DOWNLOAD { get; set; }
        public Nullable<int> SP_CURR_DOWNLOAD { get; set; }
        public string SP_PATH { get; set; }
        public string SP_STATUS { get; set; }
        public Nullable<System.DateTime> CREATED_AT { get; set; }
    
        public virtual IB_BOOK_TEACHER IB_BOOK_TEACHER { get; set; }
        public virtual IB_TEACHER IB_TEACHER { get; set; }
    }
}
