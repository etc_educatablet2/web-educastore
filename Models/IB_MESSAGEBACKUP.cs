//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class IB_MESSAGEBACKUP
    {
        public decimal ID_MESSAGE { get; set; }
        public string ID_SENDER { get; set; }
        public string MESSAGE { get; set; }
        public decimal ID_STATUS { get; set; }
        public Nullable<decimal> ID_MESSAGETYPE { get; set; }
        public Nullable<System.DateTime> ME_CREATEDDATE { get; set; }
        public Nullable<decimal> ID_ATTACHMENT { get; set; }
        public Nullable<decimal> ID_SECTION { get; set; }
        public Nullable<decimal> ID_COURSE { get; set; }
        public Nullable<decimal> ID_ATTACHMENTTEACHER { get; set; }
    }
}
