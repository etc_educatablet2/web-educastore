//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ET_QUESTION_TYPE
    {
        public ET_QUESTION_TYPE()
        {
            this.ET_QUESTION = new HashSet<ET_QUESTION>();
        }
    
        public decimal ID_QUESTION_TYPE { get; set; }
        public string QT_NAME { get; set; }
        public string QT_ALIAS { get; set; }
        public string QT_DESCRIPTION { get; set; }
        public Nullable<System.DateTime> CREATED_AT { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_AT { get; set; }
        public string UPDATED_BY { get; set; }
    
        public virtual ICollection<ET_QUESTION> ET_QUESTION { get; set; }
    }
}
