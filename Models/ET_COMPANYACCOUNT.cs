//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ET_COMPANYACCOUNT
    {
        public ET_COMPANYACCOUNT()
        {
            this.ET_COMPANYACCOUNTHASVIRTUALPOS = new HashSet<ET_COMPANYACCOUNTHASVIRTUALPOS>();
            this.ET_TRANSFERPAYMENT = new HashSet<ET_TRANSFERPAYMENT>();
        }
    
        public decimal ID_COMPANYACCOUNT { get; set; }
        public decimal ID_BANK { get; set; }
        public decimal ID_BRAND { get; set; }
        public decimal ID_PERSON { get; set; }
        public string CA_ACCOUNT { get; set; }
        public Nullable<double> CA_FLATFEE { get; set; }
        public Nullable<double> CA_TRXFEE { get; set; }
        public string CA_STATUS { get; set; }
        public Nullable<System.DateTime> CREATED_AT { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_AT { get; set; }
        public string UPDATED_BY { get; set; }
    
        public virtual ET_BRAND ET_BRAND { get; set; }
        public virtual ET_PERSON ET_PERSON { get; set; }
        public virtual ICollection<ET_COMPANYACCOUNTHASVIRTUALPOS> ET_COMPANYACCOUNTHASVIRTUALPOS { get; set; }
        public virtual ET_PERSON ET_PERSON1 { get; set; }
        public virtual ICollection<ET_TRANSFERPAYMENT> ET_TRANSFERPAYMENT { get; set; }
    }
}
