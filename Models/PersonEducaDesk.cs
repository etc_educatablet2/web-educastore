﻿using System;
using System.Collections.Generic;
using System.Linq;
using ETC.Structures.Models;

namespace WebEducaStore.Models
{
    public class PersonEducaDesk : PersonSession
    {
        private WebEducaStoreEntities db = new WebEducaStoreEntities();

        /*
         * variables to distinguish between teacher and student
         * */
        public bool isRoleStudent { get; set; }
        public bool isRoleTeacher { get; set; }
        public Nullable<decimal> roleId { get; set; }
        public int roleInstitutionId { get; set; }

        public PersonEducaDesk(PersonSession person)
        {
            Id = person.Id;
            UserNameET = person.UserNameET;
            Identification = person.Identification;
            Email = person.Email;
            FirstName = person.FirstName;
            LastName = person.LastName;
            Fullname = person.Fullname;
            SecondLastName = person.SecondLastName;
            SecondName = person.SecondName;
            EncryptedPassword = person.EncryptedPassword;
            isActive = person.isActive;
            LibreroId = person.LibreroId;
            StoreCountryId = person.StoreCountryId;
            roles = person.roles;
        }

        /// <summary>
        /// Get additional custom information of the user
        /// </summary>
        /// <returns></returns>
        public void setCustomInfo()
        {
            /*
             * Adds the person variables required by the webstore
             * */
            if (this.roles.ContainsKey("EducaDesk"))
            {
                List<string> roles = this.roles["EducaDesk"];
                if (roles.Contains("TEACHER"))
                {
                    isRoleTeacher = true;
                    loadTeacherInfoForVm();
                }
                else if (roles.Contains("STUDENT"))
                {
                    isRoleStudent = true;
                    loadStudentInfoForVm();
                }
            }
        }

        protected void loadStudentInfoForVm()
        {
            //gets the basic info
            roleId = (from r in db.IB_STUDENT
                      where r.ST_USERNAME == Email
                      select r.AF_ID).FirstOrDefault();

            //load actual class period id for users country
            DateTime today = DateTime.Now;
            var firstOrDefault = db.ET_CLASSPERIOD.FirstOrDefault(w => w.ID_COUNTRY == StoreCountryId && w.CP_STARTDATE <= today && w.CP_ENDDATE >= today);
            if (firstOrDefault != null)
            {
                var classperiodId = firstOrDefault.ID_CLASSPERIOD;

                roleInstitutionId = (from r in db.IB_SECTION
                                     where r.IB_SECTIONHASSTUDENT.Any(w => w.ID_STUDENT == roleId)
                                     && r.ID_SCHOOLYEAR == classperiodId
                                     select r.IB_LEVEL.ID_SCHOOL).FirstOrDefault();

                //roleInstitutionId = (from r in db.ET_INSTITUTION
                //                     where r.IB_LEVEL.Any(w => w.IB_SECTION.Any(x => x.IB_SECTIONHASSTUDENT.Any(o => o.ID_STUDENT == roleId)))
                //                     select r.idinstitution).FirstOrDefault();
            }
        }

        protected void loadTeacherInfoForVm()
        {
            //gets the basic info
            roleId = (from r in db.IB_TEACHER
                      where r.AF_EMAIL == Email
                      select r.AF_ID).FirstOrDefault();

            //load actual class period id for users country
            DateTime today = DateTime.Now;
            var firstOrDefault = db.ET_CLASSPERIOD.FirstOrDefault(w => w.ID_COUNTRY == StoreCountryId && w.CP_STARTDATE <= today && w.CP_ENDDATE >= today);
            if (firstOrDefault != null)
            {
                var classperiodId = firstOrDefault.ID_CLASSPERIOD;

                roleInstitutionId = (from r in db.IB_SECTION
                                     where r.IB_SECTIONHASTEACHER.Any(w => w.ID_TEACHER == roleId)
                                     && r.ID_SCHOOLYEAR == classperiodId
                                     select r.IB_LEVEL.ID_SCHOOL).FirstOrDefault();

                //roleInstitutionId = (from r in db.ET_INSTITUTION
                //                     where r.IB_LEVEL.Any(w => w.IB_SECTION.Any(x => x.IB_SECTIONHASTEACHER.Any(o => o.ID_TEACHER == roleId)))
                //                     select r.idinstitution).FirstOrDefault();
            }


        }
    }
}