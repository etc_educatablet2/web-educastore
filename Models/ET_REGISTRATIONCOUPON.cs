//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ET_REGISTRATIONCOUPON
    {
        public decimal ID_REGISTRATIONCOUPON { get; set; }
        public decimal ID_COUPON { get; set; }
        public Nullable<decimal> ID_PERSON { get; set; }
        public Nullable<System.DateTime> RC_DATE { get; set; }
        public string RC_STATUS { get; set; }
        public Nullable<System.DateTime> CREATED_AT { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_AT { get; set; }
        public string UPDATED_BY { get; set; }
    
        public virtual ET_COUPON ET_COUPON { get; set; }
        public virtual ET_PERSON ET_PERSON { get; set; }
    }
}
