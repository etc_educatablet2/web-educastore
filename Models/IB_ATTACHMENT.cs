//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class IB_ATTACHMENT
    {
        public decimal ID_ATTACHMENT { get; set; }
        public string ID_TEACHER { get; set; }
        public string ATTACHMENT_ALIAS { get; set; }
        public string ATTACHMENT_PATH { get; set; }
        public string FILE_NAME { get; set; }
        public string ATTACHMENT_CATEGORY { get; set; }
        public Nullable<decimal> AT_SIZE { get; set; }
        public string AT_MIMETYPE { get; set; }
        public Nullable<System.DateTime> AT_DISPATCHDATE { get; set; }
        public string AT_STATUS { get; set; }
        public Nullable<System.DateTime> CREATED_AT { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_AT { get; set; }
        public string UPDATED_BY { get; set; }
        public Nullable<decimal> ID_PRODUCT { get; set; }
    
        public virtual ET_PRODUCT ET_PRODUCT { get; set; }
    }
}
