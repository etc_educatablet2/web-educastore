//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RE_SCOREVALUE
    {
        public int ID { get; set; }
        public int POINTS { get; set; }
        public int REWARDEDEVENT { get; set; }
        public int INSTITUTION { get; set; }
    
        public virtual ET_INSTITUTION ET_INSTITUTION { get; set; }
        public virtual ET_REWARDEDEVENT ET_REWARDEDEVENT { get; set; }
    }
}
