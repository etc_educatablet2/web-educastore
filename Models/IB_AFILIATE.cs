//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class IB_AFILIATE
    {
        public IB_AFILIATE()
        {
            this.IB_STUDENT = new HashSet<IB_STUDENT>();
        }
    
        public string AF_ID { get; set; }
        public string AF_NAME { get; set; }
        public string AF_STATUS { get; set; }
        public string AF_IDENTIFICATION { get; set; }
        public string AF_TELEPHONENUMBER { get; set; }
        public string AF_BIRTHDAY { get; set; }
        public string AF_EMAIL { get; set; }
        public string AF_PRODUCTNUMBER { get; set; }
        public string AF_CLIENTSINCE { get; set; }
        public string AF_CPEOPLE { get; set; }
        public string AF_PASSWORD { get; set; }
        public Nullable<System.DateTime> AF_AFILIATIONDATE { get; set; }
        public Nullable<System.DateTime> AF_STATUSDATE { get; set; }
        public string AF_TOKEN { get; set; }
        public string AF_MESSAGE { get; set; }
        public string AF_FIRSTNAME { get; set; }
        public string AF_SECONDNAME { get; set; }
        public string AF_LASTNAME { get; set; }
        public string AF_SECONDLASTNAME { get; set; }
        public string AF_COUNTRY { get; set; }
        public string AF_CITY { get; set; }
        public string AF_STREET { get; set; }
        public string AF_BUILDING { get; set; }
        public string AF_APARTMENT { get; set; }
        public string AF_ADDRESSPHONE { get; set; }
        public string AF_PHONE { get; set; }
        public string AF_RESIDENTIALAREA { get; set; }
        public string AF_MUNICIPIO { get; set; }
        public string AF_POSTALCODE { get; set; }
    
        public virtual IB_AFILIATE IB_AFILIATE1 { get; set; }
        public virtual IB_AFILIATE IB_AFILIATE2 { get; set; }
        public virtual ICollection<IB_STUDENT> IB_STUDENT { get; set; }
    }
}
