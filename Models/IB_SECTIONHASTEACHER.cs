//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class IB_SECTIONHASTEACHER
    {
        public decimal ID_SECTION { get; set; }
        public decimal ID_TEACHER { get; set; }
        public decimal ID_COURSE { get; set; }
        public decimal ID_SCHOOLYEAR { get; set; }
    
        public virtual IB_COURSE IB_COURSE { get; set; }
        public virtual IB_SCHOOLYEAR IB_SCHOOLYEAR { get; set; }
        public virtual IB_SECTION IB_SECTION { get; set; }
        public virtual IB_TEACHER IB_TEACHER { get; set; }
    }
}
