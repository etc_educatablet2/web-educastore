//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ET_SELLPRODUCTDETAIL
    {
        public decimal ID_SELLPRODUCTDETAIL { get; set; }
        public decimal ID_PRODUCT { get; set; }
        public decimal ID_SELL { get; set; }
        public Nullable<int> SD_QUANTITY { get; set; }
        public Nullable<double> SD_PRICE { get; set; }
        public Nullable<System.DateTime> CREATED_AT { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_AT { get; set; }
        public string UPDATED_BY { get; set; }
        public Nullable<decimal> ID_BOOK_OLD { get; set; }
    
        public virtual ET_PRODUCT ET_PRODUCT { get; set; }
        public virtual ET_SELLPRODUCT ET_SELLPRODUCT { get; set; }
    }
}
