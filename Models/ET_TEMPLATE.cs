//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ET_TEMPLATE
    {
        public ET_TEMPLATE()
        {
            this.ET_TEMPLATE_ASSIGN = new HashSet<ET_TEMPLATE_ASSIGN>();
            this.ET_TEMPLATE_ANSWER = new HashSet<ET_TEMPLATE_ANSWER>();
            this.ET_TEMPLATE_QUESTIONS = new HashSet<ET_TEMPLATE_QUESTIONS>();
        }
    
        public decimal ID_TEMPLATE { get; set; }
        public decimal ID_SECTION { get; set; }
        public decimal ID_COURSE { get; set; }
        public decimal ID_LEVEL { get; set; }
        public decimal ID_PERSON { get; set; }
        public decimal ID_TEMPLATE_STATUS { get; set; }
        public string TM_TITLE { get; set; }
        public string TM_HEADER { get; set; }
        public string TM_DESCRIPTION { get; set; }
        public Nullable<bool> TM_IS_VISIBLE { get; set; }
        public Nullable<decimal> TM_TYPE { get; set; }
        public Nullable<decimal> TM_DURATION { get; set; }
        public Nullable<System.DateTime> TM_ENDDATE { get; set; }
        public Nullable<decimal> TM_NUMBER_RANDOM_QUESTIONS { get; set; }
        public Nullable<System.DateTime> CREATED_AT { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_AT { get; set; }
        public string UPDATED_BY { get; set; }
    
        public virtual ET_PERSON ET_PERSON { get; set; }
        public virtual ET_TEMPLATE_STATUS ET_TEMPLATE_STATUS { get; set; }
        public virtual ICollection<ET_TEMPLATE_ASSIGN> ET_TEMPLATE_ASSIGN { get; set; }
        public virtual ICollection<ET_TEMPLATE_ANSWER> ET_TEMPLATE_ANSWER { get; set; }
        public virtual ICollection<ET_TEMPLATE_QUESTIONS> ET_TEMPLATE_QUESTIONS { get; set; }
    }
}
