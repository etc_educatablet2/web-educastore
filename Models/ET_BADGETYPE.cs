//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ET_BADGETYPE
    {
        public ET_BADGETYPE()
        {
            this.BT_UPGRADERATE = new HashSet<BT_UPGRADERATE>();
            this.ET_BADGE = new HashSet<ET_BADGE>();
            this.ET_REWARDEDEVENT = new HashSet<ET_REWARDEDEVENT>();
        }
    
        public int ID { get; set; }
        public string NAME { get; set; }
    
        public virtual ICollection<BT_UPGRADERATE> BT_UPGRADERATE { get; set; }
        public virtual ICollection<ET_BADGE> ET_BADGE { get; set; }
        public virtual ICollection<ET_REWARDEDEVENT> ET_REWARDEDEVENT { get; set; }
    }
}
