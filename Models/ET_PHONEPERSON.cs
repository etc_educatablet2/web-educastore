//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ET_PHONEPERSON
    {
        public decimal ID_PHONEPERSON { get; set; }
        public decimal ID_PERSON { get; set; }
        public decimal ID_PHONETYPE { get; set; }
        public string PP_PHONENUMBER { get; set; }
        public string PP_STATUS { get; set; }
        public Nullable<System.DateTime> CREATED_AT { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_AT { get; set; }
        public string UPDATED_BY { get; set; }
    
        public virtual ET_PERSON ET_PERSON { get; set; }
        public virtual ET_PHONETYPE ET_PHONETYPE { get; set; }
    }
}
