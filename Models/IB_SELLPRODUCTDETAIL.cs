//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class IB_SELLPRODUCTDETAIL
    {
        public decimal ID_DETAIL { get; set; }
        public decimal ID_SELL { get; set; }
        public decimal ID_PRODUCT { get; set; }
        public decimal ID_BOOK { get; set; }
        public Nullable<decimal> DE_PRICE { get; set; }
    
        public virtual IB_PRECIO IB_PRECIO { get; set; }
        public virtual IB_SELLPRODUCT IB_SELLPRODUCT { get; set; }
    }
}
