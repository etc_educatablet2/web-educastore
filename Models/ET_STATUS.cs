//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebEducaStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ET_STATUS
    {
        public ET_STATUS()
        {
            this.ET_REVIEW = new HashSet<ET_REVIEW>();
        }
    
        public decimal ID_STATUS { get; set; }
        public string STATUS_DESCRIPTION { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_AT { get; set; }
        public string UPDATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_AT { get; set; }
    
        public virtual ICollection<ET_REVIEW> ET_REVIEW { get; set; }
    }
}
