﻿using System.Web;
using System.Web.Mvc;
using WebEducaStore.Controllers;
using WebEducaStore.Models;

namespace WebEducaStore
{
  public class FilterConfig
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
    {
      filters.Add(new HandleErrorAttribute());
      filters.Add(new ValidateInputAttribute(false));
      filters.Add(new VerifyAuthFilterAttribute());
    }
  }
}