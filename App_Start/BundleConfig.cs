﻿using System.Web;
using System.Web.Optimization;

namespace WebEducaStore
{
  public class BundleConfig
  {
    // Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
      public static void RegisterBundles(BundleCollection bundles)
      {
          bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/Scripts/jquery-{version}.js"));

          bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                      "~/Scripts/jquery-ui-{version}.js"));

          bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                      "~/Scripts/jquery.unobtrusive*",
                      "~/Scripts/jquery.validate*"));

          // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información sobre los formularios. De este modo, estará
          // preparado para la producción y podrá utilizar la herramienta de creación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
          bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                      "~/Scripts/modernizr-*"));

          bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

          bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                      "~/Content/themes/base/jquery.ui.core.css",
                      "~/Content/themes/base/jquery.ui.resizable.css",
                      "~/Content/themes/base/jquery.ui.selectable.css",
                      "~/Content/themes/base/jquery.ui.accordion.css",
                      "~/Content/themes/base/jquery.ui.autocomplete.css",
                      "~/Content/themes/base/jquery.ui.button.css",
                      "~/Content/themes/base/jquery.ui.dialog.css",
                      "~/Content/themes/base/jquery.ui.slider.css",
                      "~/Content/themes/base/jquery.ui.tabs.css",
                      "~/Content/themes/base/jquery.ui.datepicker.css",
                      "~/Content/themes/base/jquery.ui.progressbar.css",
                      "~/Content/themes/base/jquery.ui.theme.css",
                      "~/Content/themes/base/jquery-ui-1.10.4.css"));

          bundles.Add(new StyleBundle("~/Content/flatapp/css").Include(
              //Bootstrap is included in its original form, unaltered
            "~/Content/flatapp/bootstrap.css",
              //Related styles of various icon packs and javascript plugins
            "~/Content/flatapp/plugins.css",
              //The main stylesheet of this template. All Bootstrap overwrites are defined in here
            "~/Content/flatapp/main.css"));

          bundles.Add(new ScriptBundle("~/Scripts/flatapp/modernizr").Include(
              //Modernizr (Browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it)
            "~/Scripts/flatapp/vendor/modernizr-2.6.2-respond-1.1.0.min.js"));

          bundles.Add(new ScriptBundle("~/Scripts/flatapp/bootstrap").Include(
            "~/Scripts/flatapp/vendor/bootstrap.min.js"));

          bundles.Add(new ScriptBundle("~/Scripts/flatapp/general").Include(
            "~/Scripts/flatapp/plugins.js",
            "~/Scripts/flatapp/main.js"));

          bundles.Add(new ScriptBundle("~/Scripts/general").Include(
            "~/Scripts/jquery.unobtrusive-ajax.js"));
      }
  }
}